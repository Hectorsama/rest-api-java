<!DOCTYPE html>
<html lang="es" dir="ltr">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <head>
    <style type="text/css">
      @import url('https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,700;1,500&display=swap');
      .header{g
        display: flex;
        justify-content: center;
      }

      .footer{
        display: flex;
        justify-content: center;
      }
      .footer-text{
        display: flex;
        justify-content: center;
        font-family: 'Mulish', sans-serif;
      }

      .footer img{
        max-width: 200px;
      }

      .titulo-sub{
        display: flex;
        justify-content: center;
        font-family: 'Mulish', sans-serif;
        color: #DAB66B;
      }
      .sub{
        display: flex;
        justify-content: center;
        font-family: 'Mulish', sans-serif;
        color: #DAB66B;
      }

.succes {
    display: flex;
    justify-content: center;
    font-family: 'Mulish', sans-serif;
    color: #333333;
    margin-left: 10%;
    margin-right: 10%;
}
      .date {
          margin-left: 10%;
          font-family: 'Mulish', sans-serif;
          color: #333333;
          font-size: 15px;
          margin-top: 5%;
          text-align: left;
      }



      .order{
        font-family: 'Mulish', sans-serif;
        color: #333333;
        font-size: 20px;
        margin-left: 10%;
      }


      .name {
          font-family: 'Mulish', sans-serif;
          color: #333333;
          font-size: 15px;
          margin-left: 10%;
          margin-right: 10%;
          text-align: left;
      }

      .name-recordatorio {
          font-family: 'Mulish', sans-serif;
          color: #FE6D73;
          font-size: 10px;
          margin-left: 10%;
          margin-right: 10%;
          text-align: left;
      }
      .contact-c{
        font-family: 'Mulish', sans-serif;
        color: #333333;
        font-size: 18px;
        margin-left: 10%;
        margin-top: 5%;
        color: #DAB66B;
      }
      .name-din {
          font-family: 'Mulish', sans-serif;
          color: #333333;
          font-size: 15px;
          margin-left: 23%;
          margin-top: -35px;
      }


      .price{
        font-family: 'Mulish', sans-serif;
        color: #333333;
        font-size: 20px;
        margin-left: 70%;
        margin-top: 5%;
      }

      .price-num{
        font-family: 'Mulish', sans-serif;
        color: #333333;
        font-size: 20px;
        margin-left: 70%;
        margin-top: 10%;
      }

      .line{
        margin-left:65%;
      }

      .total-price{
        font-family: 'Mulish', sans-serif;
        color: #333333;
        font-size: 20px;
        margin-left: 55%;
      }


      h1{
        font-size: 70px;
      }

      h3 {
          font-size: 30px;
          text-align: center;
      }
    </style>
    <meta charset="utf-8">
    <title>Correo para solictud de un recinto con paquete</title>
    <link rel="stylesheet" href="style.css" media="screen" />
  </head>
  <body>

    <div class="header">
      <img class="globitos"src="https://public-photos.s3.amazonaws.com/Globos.png">
    </div>
    <center>
    <div class="titulo-sub">
      <h1>PartyOn</h1>
    </div>
    <div class="sub">
      <h2>Tu fiesta cuando quieras, donde quieras, donde quieras. </h2>
    </div>
  </center>
    <div class="succes">
      <h3>Tu recinto se registro correctamente</h3>
    </div>


    <br>
 <p class="name"> El registro será verificado una vez se haya hecho el pago y validado los datos, </p>
    <p class="name"> Nombre del Salón:${name_place}</p>
    <p class="name"> Recuerda que una vez realizado el pago,deberás enviar un correo a
     help@partyon.mx con un archivo adjunto , que será tu comprobante  de pago y nombre del recinto registrado.
     </p>

    <br>
    <p class="contact-c"> Datos del pago por paquete:</p>
    <p class="name"> Cantidad a Transferir:  ${price}</p>
    <p class="name"> Banco destino:  BBVA Bancomer</p>
    <p class="name"> Cuenta CLABE a transferir:  012180001157219460</p>
    <p class="name-recordatorio"> Recuerda que solo tienes 24 hrs para concretar el pago.</p>

    <br>

<br>
<br>
<br>
    <footer>
      <center>
      <div class="footer">
        <img class="globitos"src="https://public-photos.s3.amazonaws.com/Globos.png">
      </div>
      <div class="footer-text">
        <p>Los servicios brindados a discreción del proveedor en previo acuerdo con el presente usuario.</p>
      </div>
    </center>
    </footer>
  </body>
</html>
