<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <style type="text/css">
      @import url('https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,700;1,500&display=swap');
      .header{
        display: flex;
        justify-content: center;
      }

      .footer{
        display: flex;
        justify-content: center;
      }
      .footer-text{
        display: flex;
        justify-content: center;
        font-family: 'Mulish', sans-serif;
      }

      .footer img{
        width: 200px;
      }

      .titulo-sub{
        display: flex;
        justify-content: center;
        font-family: 'Mulish', sans-serif;
        color: #DAB66B;
      }
      .sub{
        display: flex;
        justify-content: center;
        font-family: 'Mulish', sans-serif;
        color: #DAB66B;
      }

      .succes{
        display: flex;
        justify-content: center;
        font-family: 'Mulish', sans-serif;
        color: #333333;
        margin-left: 10%
      }
      .date{
      margin-left: 65%;
        font-family: 'Mulish', sans-serif;
        color: #333333;
        font-size: 20px;
        margin-top: 5%;
      }



      .order{
        font-family: 'Mulish', sans-serif;
        color: #333333;
        font-size: 20px;
        margin-left: 10%;
      }


      .name{
        font-family: 'Mulish', sans-serif;
        color: #333333;
        font-size: 15px;
        margin-left: 10%;
        margin-top: 2%;
      }
      .contact-c{
        font-family: 'Mulish', sans-serif;
        color: #333333;
        font-size: 18px;
        margin-left: 10%;
        margin-top: 5%;
      }
      .name-din {
          font-family: 'Mulish', sans-serif;
          color: #333333;
          font-size: 15px;
          margin-left: 23%;
          margin-top: -35px;
      }


      .price{
        font-family: 'Mulish', sans-serif;
        color: #333333;
        font-size: 20px;
        margin-left: 70%;
        margin-top: 5%;
      }

      .price-num{
        font-family: 'Mulish', sans-serif;
        color: #333333;
        font-size: 20px;
        margin-left: 70%;
        margin-top: 10%;
      }

      .line{
        margin-left:65%;
      }

      .total-price{
        font-family: 'Mulish', sans-serif;
        color: #333333;
        font-size: 20px;
        margin-left: 55%;
      }


      h1{
        font-size: 70px;
      }

      h3{
        font-size: 40px;
      }
    </style>
    <meta charset="utf-8">
    <title>Formato para correo Proveedor</title>
    <link rel="stylesheet" href="style.css" media="screen" />
  </head>
  <body>

    <div class="header">
      <img class="globitos"src="https://public-photos.s3.amazonaws.com/Globos.png">
    </div>
    <center>
    <div class="titulo-sub">
      <h1>PartyOn</h1>
    </div>
    <div class="sub">
      <h2>Tu fiesta cuando quieras, donde quieras, donde quieras. </h2>
    </div>
  </center>
    <div class="succes">
      <h2>¡Se ha concretado la reserva para el día ${dia} de ${mes} de ${year}!</h2>
    </div>
    <br>
    <p class="name"> Nombre del Salón: ${name_place} </p>
    <p class="name"> Precio del recinto:  ${price}</p>
    <p class="name"> Cargo PartyOn:  ${pricePartyOn}</p>
    <br>
    <p class="name"> Total para reservar:  ${priceReservation}</p>
    <br>
    <p class="contact-c"> Datos del cliente:</p>
    <p class="name"> Nombre: ${nameApplicant}</p>
    <p class="name"> Correo: ${mail} </p>
<br>
<br>
<br>
    <footer>
      <center>
      <div class="footer">
        <img class="globitos"src="https://public-photos.s3.amazonaws.com/Globos.png">
      </div>
      <div class="footer-text">
        <p>Los servicios se suministrarán al usuario según los acuerdos con el presente proveedor.</p>
      </div>
    </center>
    </footer>
  </body>
</html>
