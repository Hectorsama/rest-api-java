package com.partyOn.partyOn_backend.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.ResourceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import javax.annotation.PostConstruct;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Configuration
public class FirebaseConfig {

	@Bean
	public DatabaseReference firebaseDatabse() {
		DatabaseReference firebase = FirebaseDatabase.getInstance().getReference();
		return firebase;
	}

	@Value("${rs.pscode.firebase.database.url}")
	private String databaseUrl;
	
	@Value("${firebase.json.url}")
	private String jsonUrl;

	@PostConstruct
	public void init() throws IOException {

		/**
		 * https://firebase.google.com/docs/server/setup
		 *
		 * Create service account , download json
		 */
		/**
		 * Asi funciona para la instancia en Elastic Beanstalk,. 
		 */
		//InputStream configPath = new ClassPathResource("partyon-a3609-firebase-adminsdk-ac0z9-731bfc443f.json").getInputStream();
		InputStream configPath = new ClassPathResource(jsonUrl).getInputStream();

		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredentials(GoogleCredentials.fromStream(configPath)).setDatabaseUrl(databaseUrl).build();
		if (FirebaseApp.getApps().isEmpty()) {
			FirebaseApp.initializeApp(options);
		}

	}
}
