package com.partyOn.partyOn_backend.config.exception;


import org.springframework.security.BadCredentialsException;

public class FirebaseTokenInvalidException extends BadCredentialsException {

    public FirebaseTokenInvalidException(String msg) {
        super(msg);
    }

}