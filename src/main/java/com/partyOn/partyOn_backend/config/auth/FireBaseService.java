package com.partyOn.partyOn_backend.config.auth;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.repo.UserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class FireBaseService implements Firebase {
	
	
    @Override
    public TokenInfo parseToken(String firebaseToken) {
    	
    	
        if (StringUtils.isEmpty(firebaseToken)) {
            throw new IllegalArgumentException("Firebase Token Blank");
        }
        FirebaseToken token;
        TokenInfo tokeninfo = new TokenInfo();
        try {
            token = FirebaseAuth.getInstance().verifyIdToken(firebaseToken);
            tokeninfo.setValidated(true);
            tokeninfo.setUid(token.getUid());
            return tokeninfo;
        } catch (Exception e) {
            throw new BadCredentialsException(e.getMessage());
        }

    }

}
