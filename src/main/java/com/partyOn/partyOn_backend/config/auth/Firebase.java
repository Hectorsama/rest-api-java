package com.partyOn.partyOn_backend.config.auth;

public interface Firebase {
    TokenInfo parseToken(String idToken) throws Exception;
}
