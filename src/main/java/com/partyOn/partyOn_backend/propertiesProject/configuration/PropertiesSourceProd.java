package com.partyOn.partyOn_backend.propertiesProject.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Configuration

@PropertySource({"classpath:prod.properties"})
@Profile("prod")
public class PropertiesSourceProd {

}
