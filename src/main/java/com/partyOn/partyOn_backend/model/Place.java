/*
 * COPYRIGHT  (c) 2020.  PartyOn - ALL RIGHTS RESERVED
 * Author: Luis Gerardo Bernabe Gomez & Hector Santaella Marin
 */

package com.partyOn.partyOn_backend.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "lugares")
public class Place {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nombre")
    private String name;

    @Column(name = "descripcion")
    private String description;

    @Column(name = "latitud")
    private Double latitude;

    @Column(name = "longitud")
    private Double longitude;

    @Column(name = "ciudad")
    private String city;

    @Column(name = "estado")
    private String state;

    @Column(name = "municipio_delegacion")
    private String municipality;

    @Column(name = "cp")
    private String cp;

    @Column(name = "num_interior")
    private String internalNumber;

    @Column(name = "num_exterior")
    private String externalNumber;

    @Column(name = "num_likes")
    private Long likes;

    @Column(name = "costo")
    private BigDecimal price;

    @Column(name = "capacidad")
    private int capacity;

    @Column(name = "rating")
    private Double rating;

    @Column(name = "estatus")
    private String status;

    @Column(name = "num_strikes")
    private int numStrikes;

    @Column(name = "pais")
    private String country;

    @Column(name = "zona_horaria")
    private String timeZone;

    @Column(name = "calle")
    private String street;

    @Column(name = "porcentaje")
    private Double percentage;


    @Column(name = "cuenta_clabe")
    private String clabe;

    @ManyToMany(mappedBy = "likes")
    private List<User> favoritos = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "propiedad_id", nullable = false)
    private PropertyType propertyType;

    @OneToMany(mappedBy = "place", cascade = CascadeType.ALL)
    private List<BusyDay> busyDays = new ArrayList<>();

    @OneToMany(mappedBy = "place", cascade = CascadeType.ALL)
    private List<Photos> photos = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "propietario_id", nullable = false)
    private User user;

    @OneToMany(mappedBy = "requestedPlace", cascade = CascadeType.ALL)
    private List<Reservation> reservations = new ArrayList<Reservation>();

    @JoinTable(name = "categoria_lugar", joinColumns = @JoinColumn(name = "lugar_id", nullable = false), inverseJoinColumns = @JoinColumn(name = "categoria_id", nullable = false))
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Category> categories = new ArrayList<>();

    @JoinTable(name = "lugar_servicio", joinColumns = @JoinColumn(name = "lugar_id", nullable = false), inverseJoinColumns = @JoinColumn(name = "servicio_id", nullable = false))
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Services> services = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "paquete_id", nullable = false)
    private PackageParty packageParty; // package es palabra reservada jajaja

    @OneToMany(mappedBy = "place", cascade = CascadeType.ALL)
    private List<Valuation> valuations = new ArrayList<>();

    public List<Valuation> AllValuations() {
        return valuations;
    }

    public void setValuations(List<Valuation> valuations) {
        this.valuations = valuations;
    }

    public PackageParty findPackageParty() {
        return packageParty;
    }

    public void setPackageParty(PackageParty packageParty) {
        this.packageParty = packageParty;
    }

    public PropertyType getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType) {
        this.propertyType = propertyType;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public List<Reservation> allReservations() {
        return this.reservations;
    }

    public void setBusyDays(List<BusyDay> busyDays) {

        this.busyDays = busyDays;
    }

    public void setPhotos(List<Photos> photos) {
        this.photos = photos;
    }

    public void deleteFavorito(User user) {
        this.favoritos.remove(user);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User findUser() {
        return user;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getInternalNumber() {
        return internalNumber;
    }

    public void setInternalNumber(String internal_number) {
        this.internalNumber = internal_number;
    }

    public String getExternalNumber() {
        return externalNumber;
    }

    public void setExternal_number(String external_number) {
        this.externalNumber = external_number;
    }

    public Long getLikes() {
        return likes;
    }

    public void setLikes(Long likes) {
        this.likes = likes;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public List<BusyDay> allBusyDays() {
        return busyDays;
    }

    public void insertBusyDay(BusyDay busyDay) {
        this.busyDays.add(busyDay);
    }

    public List<Photos> getPhotos() {
        return photos;
    }

    public List<User> allFavoritos() {
        return this.favoritos;
    }

    public void setFavoritos(List<User> favoritos) {
        this.favoritos = favoritos;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public List<Category> setCategories(List<Category> categories) {
        return this.categories = categories;
    }

    public List<Services> getServices() {
        return services;
    }

    public List<Services> setServices(List<Services> services) {
        return this.services = services;
    }

    public void insert_category(Category category) {
        this.categories.add(category);
    }

    public void insert_service(Services services) {
        this.services.add(services);
    }

    public void insert_photos(Photos photos) {
        this.photos.add(photos);
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNumStrikes() {
        return numStrikes;
    }

    public void setNumStrikes(int numStrikes) {
        this.numStrikes = numStrikes;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    public String getClabe() {
        return clabe;
    }

    public void setClabe(String clabe) {
        this.clabe = clabe;
    }


    public PackageParty getPackageParty() {
        return packageParty;
    }
}
