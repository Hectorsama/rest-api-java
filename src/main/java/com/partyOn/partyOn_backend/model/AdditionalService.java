/*
 * Copyright (c) 2020.
 * COPYRIGHT  (c) 2020.  PartyOn - ALL RIGHTS RESERVED
 * Author: Luis Gerardo Bernabe Gomez & Hector Santaella Marin
 */
package com.partyOn.partyOn_backend.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.mapping.Set;

@Entity
@Table(name = "servicios_adicionales")
public class AdditionalService {
	//ExtraPackage
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "sku")
	private String sku;

	@Column(name = "nombre")
	private String name;

	@Column(name = "unidades")
	private int units;

	@Column(name = "precio")
	private BigDecimal price;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "nota")
	private String note;

    @Column(name = "img_src")
    private String imgSrc;

	@Column(name = "cantidad_personas")
	private int numberPeople;

	@ManyToOne
	@JoinColumn(name = "proveedor_id")
	private Provider provider;

	@ManyToMany(mappedBy = "addtionalServices")
	private List<Events> events = new ArrayList<>();

	@OneToMany(mappedBy = "additionalService", cascade = CascadeType.ALL)
	private List<AdditionalServiceReservation> additionalServiceReservations = new ArrayList<>();

	@JoinTable(name = "trans_servicios_categorias", joinColumns = @JoinColumn(name = "servicio_adicional_id", nullable = false), inverseJoinColumns = @JoinColumn(name = "categoria_servicio_adicional_id", nullable = false))
    @ManyToMany(cascade = CascadeType.ALL)
	private List<AdditionalServiceCategory> categories=new ArrayList<>();
	
	public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }


    public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Events> AllEvents() {
		return events;
	}

	public void setEvents(List<Events> events) {
		this.events = events;
	}

	public void removeEvent(Events event) {
		this.events.remove(event);
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getUnits() {
		return units;
	}

	public void setUnits(int units) {
		this.units = units;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<AdditionalServiceReservation> findAdditionalServiceReservations() {
		return additionalServiceReservations;
	}

	public void setAdditionalServiceReservations(List<AdditionalServiceReservation> additionalServiceReservations) {
		this.additionalServiceReservations = additionalServiceReservations;
	}
}
