package com.partyOn.partyOn_backend.model;


public class TokenOpenpay {
    String id;
    Card card;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @Override
    public String toString() {
        return "TokenOpenpay{" +
                "id='" + id + '\'' +
                ", card=" + card +
                '}';
    }
}
