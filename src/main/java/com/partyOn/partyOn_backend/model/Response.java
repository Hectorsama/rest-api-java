/*
 * Copyright (c) 2020.
 * COPYRIGHT  (c) 2020.  PartyOn - ALL RIGHTS RESERVED
 * Author: Luis Gerardo Bernabe Gomez & Hector Santaella Marin
 */

package com.partyOn.partyOn_backend.model;

public class Response {
    private String message;
    private String status;

    public Response(String message, String status) {
        this.message = message;
        this.status = status;
    }


    public Response() {
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
