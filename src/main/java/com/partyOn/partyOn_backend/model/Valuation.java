package com.partyOn.partyOn_backend.model;

import javax.persistence.*;

@Entity
@Table(name = "valoraciones")
public class Valuation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	

	@ManyToOne
	@JoinColumn(name = "lugar_id")
	private Place place;
	
	@ManyToOne
	@JoinColumn(name = "evento_id")
	private Events event;

	@Column(name = "rating")
	private int rating;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public Events getEvent() {
		return event;
	}

	public void setEvent(Events event) {
		this.event = event;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
}
