/*
 * Copyright (c) 2020.
 * COPYRIGHT  (c) 2020.  PartyOn - ALL RIGHTS RESERVED
 * Author: Luis Gerardo Bernabe Gomez & Hector Santaella Marin
 */

package com.partyOn.partyOn_backend.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "reservaciones")
public class Reservation {
//    public static final int ES_MI_RESERVACION = 1;
//    public static final int SOY_INVITADO = 2;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cantidad_horas")
    private int numberHours;

    @Column(name = "costo_total")
    private BigDecimal totalCost;

    @Column(name = "dia_reservacion")
    private OffsetDateTime reservationDay;

    @Column(name = "dia_evento")
    private OffsetDateTime eventDay;

    @Column(name = "estatus")
    private String status;

    @Column(name = "nota")
    private String note;

    @Column(name = "tipo_reservacion")
    private int reservationType;

    @ManyToOne
    @JoinColumn(name = "usuario_id")
    private User applicant;

    @ManyToOne
    @JoinColumn(name = "lugar_id")
    private Place requestedPlace;

//    @OneToOne(mappedBy = "reservation", cascade = CascadeType.ALL)
//    private Events event;

//    @OneToOne(mappedBy = "reservation", cascade = CascadeType.ALL)
//    private Payment payment;

    @Column(name = "cargo_partyon")
    private BigDecimal chargePartyOn;

    @Column(name = "cargo_recinto")
    private BigDecimal chargeEnclosure;

//    public Events getEvent() {
//        return event;
//    }
//
//    public void setEvent(Events event) {
//        this.event = event;
//    }
//
//    public Events eventConfirmated() {
//        return this.event;
//    }

    public Place getRequestedPlace() {
        return requestedPlace;
    }

    public void setRequestedPlace(Place requestedPlace) {
        this.requestedPlace = requestedPlace;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumberHours() {
        return numberHours;
    }

    public void setNumberHours(int numberHours) {
        this.numberHours = numberHours;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public OffsetDateTime getReservationDay() {
        return reservationDay;
    }

    public void setReservationDay(OffsetDateTime reservationDay) {
        this.reservationDay = reservationDay;
    }

    public OffsetDateTime getEventDay() {
        return eventDay;
    }

    public void setEventDay(OffsetDateTime eventDay) {
        this.eventDay = eventDay;
    }

    public User findApplicant() {
        return this.applicant;
    }

    public void setApplicant(User applicant) {
        this.applicant = applicant;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public BigDecimal getChargePartyOn() {
        return chargePartyOn;
    }

    public void setChargePartyOn(BigDecimal chargePartyOn) {
        this.chargePartyOn = chargePartyOn;
    }

    public BigDecimal getChargeEnclosure() {
        return chargeEnclosure;
    }

    public void setChargeEnclosure(BigDecimal chargeEnclosure) {
        this.chargeEnclosure = chargeEnclosure;
    }

    public int getReservationType() {
        return reservationType;
    }

    public void setReservationType(int reservationType) {
        this.reservationType = reservationType;
    }
}
