/*
 * Copyright (c) 2020.
 * COPYRIGHT  (c) 2020.  PartyOn - ALL RIGHTS RESERVED
 * Author: Luis Gerardo Bernabe Gomez & Hector Santaella Marin
 */

package com.partyOn.partyOn_backend.model;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "pagos")
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "numero_orden")
    private String orderNumber;

    @Column(name = "fecha_pago")
    private OffsetDateTime payDay;

    @ManyToOne
    @JoinColumn(name = "usuario_id", nullable = false, updatable = false)
    private User user;

    @OneToOne
    @JoinColumn(name = "reservacion_id", nullable = false, updatable = false)
    private Reservation reservation;


    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public OffsetDateTime getPayDay() {
        return payDay;
    }

    public void setPayDay(OffsetDateTime payDay) {
        this.payDay = payDay;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }
}
