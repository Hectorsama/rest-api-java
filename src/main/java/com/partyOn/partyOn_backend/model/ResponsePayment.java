/*
 * Copyright (c) 2020.
 * COPYRIGHT  (c) 2020.  PartyOn - ALL RIGHTS RESERVED
 * Author: Luis Gerardo Bernabe Gomez & Hector Santaella Marin
 */

package com.partyOn.partyOn_backend.model;

public class ResponsePayment {
    private String id;
    private String authorization;
    private String status;
    private String amount;
    private String card;

    public ResponsePayment(String id, String authorization, String status, String amount, String card) {
        this.id = id;
        this.authorization = authorization;
        this.status = status;
        this.amount = amount;
        this.card = card;

    }

    public ResponsePayment() {
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getAmount() {
        return amount;
    }

    public String getAuthorization() {
        return authorization;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}
