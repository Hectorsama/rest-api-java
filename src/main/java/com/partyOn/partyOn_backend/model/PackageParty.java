package com.partyOn.partyOn_backend.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "paquetes")
public class PackageParty {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "suscripcion_anual")
	private BigDecimal annualSubscription;
	
	@Column(name = "fotos_propias")
	private String ownPhoto;
	
	@Column(name = "fotos_profesionales")
	private String professionaPhotos;
	
	@Column(name ="video")
	private String video;

	@Column(name = "atencion_personal")
	private String personalAttention;

	@Column(name = "nombre")
	private String name;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getAnnualSubscription() {
		return annualSubscription;
	}

	public void setAnnualSubscription(BigDecimal annualSubscription) {
		this.annualSubscription = annualSubscription;
	}

	public String getOwnPhoto() {
		return ownPhoto;
	}

	public void setOwnPhoto(String ownPhoto) {
		this.ownPhoto = ownPhoto;
	}

	public String getProfessionaPhotos() {
		return professionaPhotos;
	}

	public void setProfessionaPhotos(String professionaPhotos) {
		this.professionaPhotos = professionaPhotos;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public String getPersonalAttention() {
		return personalAttention;
	}

	public void setPersonalAttention(String personalAttention) {
		this.personalAttention = personalAttention;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
