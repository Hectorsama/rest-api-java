package com.partyOn.partyOn_backend.model;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "pago_lugar")
public class PaymentPlace {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "orden_pago")
    private String orderNumber;

    @Column(name = "fecha_pago")
    private OffsetDateTime payDay;

    @OneToOne
    @JoinColumn(name = "lugar_id", nullable = false, updatable = false)
    private Place place;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public OffsetDateTime getPayDay() {
        return payDay;
    }

    public void setPayDay(OffsetDateTime payDay) {
        this.payDay = payDay;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

}
