package com.partyOn.partyOn_backend.model.firebase;

public class ReservacionNotificacion extends Notificacion {

	private Long idReservacion;
	private String photoApplicant;
	private Long idApplicant;

	public Long getIdApplicant() {
		return idApplicant;
	}

	public void setIdApplicant(Long idApplicant) {
		this.idApplicant = idApplicant;
	}

	public ReservacionNotificacion(Long nwIdReservacion) {
		this.idReservacion = nwIdReservacion;

	}

	public ReservacionNotificacion(Long nwIdReservacion, String nwPhotoApplicant) {
		this.idReservacion = nwIdReservacion;
		this.photoApplicant = nwPhotoApplicant;
	}

	public Long getIdReservacion() {
		return idReservacion;
	}

	public void setIdReservacion(Long idReservacion) {
		this.idReservacion = idReservacion;
	}

	public String getPhotoApplicant() {
		return photoApplicant;
	}

	public void setPhotoApplicant(String photoApplicant) {
		this.photoApplicant = photoApplicant;
	}
}
