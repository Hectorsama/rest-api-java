package com.partyOn.partyOn_backend.model.firebase;

public class Mensaje {
//	private String id;
    private String msg;
//    private String name;
//    private String photoUrl;
//    private String imageUrl;
//    private String hour;
    private String date;
    private String keyEmisor;
    private String idOwner;
    private  String idApplicant;
    
    
	public String getIdOwner() {
		return idOwner;
	}
	public void setIdOwner(String idOwner) {
		this.idOwner = idOwner;
	}
	public String getIdApplicant() {
		return idApplicant;
	}
	public void setIdApplicant(String idApplicant) {
		this.idApplicant = idApplicant;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getKeyEmisor() {
		return keyEmisor;
	}
	public void setKeyEmisor(String keyEmisor) {
		this.keyEmisor = keyEmisor;
	}
    
   
    
}
