package com.partyOn.partyOn_backend.model.firebase;

public class AdditionalServiceNotificacion extends Notificacion {
	Long idAdditionalService;
	String imgExtra;

	public AdditionalServiceNotificacion(Long nwIdAdditionalService) {
		this.idAdditionalService = nwIdAdditionalService;
	}

	public Long getIdAdditionalService() {
		return idAdditionalService;
	}

	public void setIdAdditionalService(Long idAdditionalService) {
		this.idAdditionalService = idAdditionalService;
	}

	public String getImgExtra() {
		return imgExtra;
	}

	public void setImgExtra(String imgExtra) {
		this.imgExtra = imgExtra;
	}

}
