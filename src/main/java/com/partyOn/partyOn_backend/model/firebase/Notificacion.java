package com.partyOn.partyOn_backend.model.firebase;

public class Notificacion {

	public static final int UNDEFINED = 0;
	public static final int NUEVO_MENSAJE_CHAT = 1;
	public static final int NUEVA_RESERVACION = 2;
	public static final int NUEVO_EXTRA_SOLICITADO = 3;
	public static final int ESTATUS_ACTUALIZADO_DE_RESERVACION = 4;

	private String body;
	private String title;
	private int tipoNotificacion;

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getTipoNotificacion() {
		return tipoNotificacion;
	}

	public void setTipoNotificacion(int tipoNotificacion) {
		this.tipoNotificacion = tipoNotificacion;
	}

	@Override
	public String toString() {
		return "body="+body+"\ntitle="+title+"\ntipoNotificacion="+tipoNotificacion;
	}

	
}
