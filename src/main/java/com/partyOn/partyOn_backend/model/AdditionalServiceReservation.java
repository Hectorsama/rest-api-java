package com.partyOn.partyOn_backend.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;

@Entity
@Table(name = "reservaciones_servicio_adicional")
public class AdditionalServiceReservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "status")
    private String status;

    @Column(name = "costo_total")
    private BigDecimal totalCost;

    @Column(name = "dia_reservacion")
    private OffsetDateTime reservationDay;

    @Column(name = "dia_evento")
    private OffsetDateTime reservationDayEvent;

    @Column(name = "nota")
    private String note;

    @Column(name = "cargo_partyon")
    private BigDecimal partyOnCharge;

    @Column(name = "cargo_proveedor")
    private BigDecimal providerCharge;

    @Column(name = "cantidad")
    private int quantity;

    @Column(name = "cp")
    private String cp;

    @ManyToOne
    @JoinColumn(name = "usuario_id")
    User user;

    @ManyToOne
    @JoinColumn(name = "servicio_adicional_id")
    AdditionalService additionalService;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public OffsetDateTime getReservationDay() {
        return reservationDay;
    }

    public void setReservationDay(OffsetDateTime reservationDay) {
        this.reservationDay = reservationDay;
    }

    public OffsetDateTime getReservationDayEvent() {
        return reservationDayEvent;
    }

    public void setReservationDayEvent(OffsetDateTime reservationDayEvent) {
        this.reservationDayEvent = reservationDayEvent;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public BigDecimal getPartyOnCharge() {
        return partyOnCharge;
    }

    public void setPartyOnCharge(BigDecimal partyOnCharge) {
        this.partyOnCharge = partyOnCharge;
    }

    public BigDecimal getProviderCharge() {
        return providerCharge;
    }

    public void setProviderCharge(BigDecimal providerCharge) {
        this.providerCharge = providerCharge;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public User findUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AdditionalService getAdditionalService() {
        return additionalService;
    }

    public void setAdditionalService(AdditionalService additionalService) {
        this.additionalService = additionalService;
    }
}
