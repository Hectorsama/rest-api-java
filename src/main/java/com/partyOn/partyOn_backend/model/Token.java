package com.partyOn.partyOn_backend.model;

public class Token {
    TokenOpenpay token;
    String device;

    public TokenOpenpay getToken() {
        return token;
    }

    public void setToken(TokenOpenpay token) {
        this.token = token;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    @Override
    public String toString() {
        return "Token{" +
                "token=" + token +
                ", device='" + device + '\'' +
                '}';
    }
}
