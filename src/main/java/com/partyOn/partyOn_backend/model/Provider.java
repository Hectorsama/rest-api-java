/*
 * Copyright (c) 2020.
 * COPYRIGHT  (c) 2020.  PartyOn - ALL RIGHTS RESERVED
 * Author: Luis Gerardo Bernabe Gomez & Hector Santaella Marin
 */

package com.partyOn.partyOn_backend.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "proveedores")
public class Provider {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nombre")
	private String name;
	@Column(name = "nombre_representante")
	private String provider_name;
	@Column(name = "correo")
	private String email;
	@Column(name = "telefono")
	private String phone;
	@Column(name = "disponibilidad")
	private String available;

	@Column(name = "img_src")
	private String imgSrc;

	@OneToMany(mappedBy = "provider")
	private List<AdditionalService> extras;

	public List<AdditionalService> AllExtras() {
		return extras;
	}

	public void setExtras(List<AdditionalService> extras) {
		this.extras = extras;
	}

	public void addExtra(AdditionalService additionalService) {
		this.extras.add(additionalService);
	}

	public String getImgSrc() {
		return imgSrc;
	}

	public void setImgSrc(String imgSrc) {
		this.imgSrc = imgSrc;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProvider_name() {
		return provider_name;
	}

	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAvailable() {
		return available;
	}

	public void setAvailable(String available) {
		this.available = available;
	}

	
}
