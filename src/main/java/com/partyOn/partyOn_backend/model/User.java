/*
 * COPYRIGHT  (c) 2020.  PartyOn - ALL RIGHTS RESERVED
 * Author: Luis Gerardo Bernabe Gomez & Hector Santaella Marin
 */

package com.partyOn.partyOn_backend.model;

import java.util.ArrayList;
import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuarios")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String name;

    @Column(name = "apellido_paterno")
    private String paternalSurname;

    @Column(name = "apellido_materno")
    private String maternalSurname;

    @Column(name = "telefono")
    private String phone;
    
    @Column(name = "estatus")
    private String status;

    @Column(name = "correo")
    private String email;


    @Column(name = "uid_firebase")
    private String uidFirebase;


    @Column(name = "foto_perfil")
    private String profilePhoto;

    
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Payment> payments = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    private List<Place> places;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Guest> guests = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<AdditionalServiceReservation> additionalServiceReservations = new ArrayList<>();

    @JoinTable(name = "favoritos", joinColumns = @JoinColumn(name = "usuario_id", nullable = false), inverseJoinColumns = @JoinColumn(name = "lugar_id", nullable = false))
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Place> likes = new ArrayList<>();

    @OneToMany(mappedBy = "applicant", cascade = CascadeType.ALL)
    private List<Reservation> reservations = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    private List<Promotion> promotions = new ArrayList<>();

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private SmsVerification verificationCode;


    public SmsVerification findVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(SmsVerification verificationCode) {
		this.verificationCode = verificationCode;
	}

	public List<Promotion> AllPromotions() {
        return promotions;
    }

    public void setPromotions(List<Promotion> promotions) {
        this.promotions = promotions;
    }

    public List<Place> AllPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = places;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }


    public List<Place> getLikes() {
        return likes;

    }

    public void setLikes(List<Place> likes) {
        this.likes = likes;
    }

    public void addLike(Place like) {
        this.likes.add(like);
    }

    public void dislike(Place like) {
        this.likes.remove(like);
    }


    // Si se ponen los getter si da por hecho que por default
//	regresara esta informacion en el JSON
    public List<Payment> AllPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaternalSurname() {
        return paternalSurname;
    }

    public void setPaternalSurname(String paternalSurname) {
        this.paternalSurname = paternalSurname;
    }

    public String getMaternalSurname() {
        return maternalSurname;
    }

    public void setMaternalSurname(String maternalSurname) {
        this.maternalSurname = maternalSurname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getUidFirebase() {
        return uidFirebase;
    }

    public void setUidFirebase(String uidFirebase) {
        this.uidFirebase = uidFirebase;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Guest> AllGuests() {
        return guests;
    }

    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }


    public List<AdditionalServiceReservation> findAdditionalServiceReservations() {
        return additionalServiceReservations;
    }

    public void setAdditionalServiceReservations(List<AdditionalServiceReservation> additionalServiceReservations) {
        this.additionalServiceReservations = additionalServiceReservations;
    }
}
