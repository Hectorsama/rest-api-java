package com.partyOn.partyOn_backend.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "categorias_servicio_adicional")
public class AdditionalServiceCategory {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "sub_id")
	private String subId;

	@Column(name = "nombre")
	private String name;

	@ManyToMany(mappedBy = "categories")
	private List<AdditionalService> additionalServices= new ArrayList<>();

	/*@ManyToMany(mappedBy = "extraCategories")
	private List<AdditionalService> extraPackages;*/

	/*public List<AdditionalService> AllExtraPackages() {
		return extraPackages;
	}

	public void setExtraPackages(List<AdditionalService> extraPackages) {
		this.extraPackages = extraPackages;
	}
	
	public void deleteExtraPackage(AdditionalService extraPackage) {
		this.extraPackages.remove(extraPackage);
	}*/

	
	public List<AdditionalService> allAdditionalServices() {
		return additionalServices;
	}

	public void setAdditionalServices(List<AdditionalService> additionalServices) {
		this.additionalServices = additionalServices;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
