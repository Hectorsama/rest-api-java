/*
 * Copyright (c) 2020.
 * COPYRIGHT  (c) 2020.  PartyOn - ALL RIGHTS RESERVED
 * Author: Luis Gerardo Bernabe Gomez & Hector Santaella Marin
 */

package com.partyOn.partyOn_backend.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "eventos")
public class Events {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String name = "";

    @Column(name = "descripcion")
    private String description = "";

    @OneToOne
    @JoinColumn(name = "reservacion_id", nullable = false, updatable = false)
    private Reservation reservation;

    @JoinTable(name = "servicio_adicional_evento", joinColumns = @JoinColumn(name = "evento_id", nullable = false), inverseJoinColumns = @JoinColumn(name = "servicio_adicional_id", nullable = false))
    @ManyToMany(cascade = CascadeType.ALL)
    public List<AdditionalService> addtionalServices = new ArrayList<>();

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
    private List<Valuation> valuations;

    public List<Valuation> AllValuations() {
        return valuations;
    }

    public void setValuations(List<Valuation> valuations) {
        this.valuations = valuations;
    }

    @OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
    private List<Guest> guests;

    public List<Guest> AllGuests() {
        return guests;
    }

    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }

    public List<AdditionalService> getAddtionalServices() {
        return addtionalServices;
    }

    public void setAddtionalServices(List<AdditionalService> addtionalServices) {
        this.addtionalServices = addtionalServices;
    }

    public void addAdditionalService(AdditionalService addtionalServices) {
        this.addtionalServices.add(addtionalServices);
    }

    public void removeAdditionalService(AdditionalService addtionalServices) {
        this.addtionalServices.remove(addtionalServices);
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
