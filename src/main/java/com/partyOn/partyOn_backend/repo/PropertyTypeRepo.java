package com.partyOn.partyOn_backend.repo;

import com.partyOn.partyOn_backend.model.BusyDay;
import com.partyOn.partyOn_backend.model.PropertyType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PropertyTypeRepo extends CrudRepository<PropertyType, Long> {

    @Query(value="SELECT * FROM propiedades p WHERE p.tipo = :tipo", nativeQuery = true)
    PropertyType findByTypeId(@Param("tipo") String tipo);

    @Query(value="SELECT * FROM propiedades p WHERE p.sub_id = :sub_id", nativeQuery = true)
    PropertyType findBySubId(@Param("sub_id") String sub_id);
}
