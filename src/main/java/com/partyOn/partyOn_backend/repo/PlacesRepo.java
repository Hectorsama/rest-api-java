package com.partyOn.partyOn_backend.repo;

import com.partyOn.partyOn_backend.model.Category;
import com.partyOn.partyOn_backend.model.Place;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.repository.query.Param;

import java.util.List;

@Repository
public interface PlacesRepo extends PagingAndSortingRepository<Place, Long> {

	@Query(value = "SELECT * FROM lugares p WHERE p.propietario_id = :id_propietario", nativeQuery = true)
	List<Place> findByIdPropietario(@Param("id_propietario") Long idPropietario);

	@Query(value = "SELECT * , (ACOS( COS( RADIANS( :mi_latitud ) ) " + "* COS ( RADIANS( l.latitud ) ) "
			+ "* COS( RADIANS(l.longitud) - RADIANS( :mi_longitud ) ) " + "+ SIN( RADIANS( :mi_latitud ) ) "
			+ "* SIN( RADIANS( l.latitud ) )" + ") * 6371 ) AS distance_in_km FROM lugares l WHERE l.estatus = 1 "
			+ "HAVING distance_in_km <= :radio_km " + "ORDER BY distance_in_km ASC ", nativeQuery = true)
	List<Place> findByLatLng(@Param("mi_latitud") Double latitud, @Param("mi_longitud") Double longitud,
			@Param("radio_km") int radioKm);

//Solo trae los lugares con cierta categoria
//	value = "SELECT * FROM lugares l INNER JOIN categoria_lugar cl ON cl.lugar_id = l.id INNER JOIN "
//+ "categorias c ON cl.categoria_id = c.id WHERE c.sub_id = :name"

	@Query(value = "SELECT * , (ACOS( COS( RADIANS( :mi_latitud ) ) " + "* COS ( RADIANS( l.latitud ) ) "
			+ "* COS( RADIANS(l.longitud) - RADIANS( :mi_longitud ) ) " + "+ SIN( RADIANS( :mi_latitud ) ) "
			+ "* SIN( RADIANS( l.latitud ) )"
			+ ") * 6371 ) AS distance_in_km FROM lugares l "+"INNER JOIN categoria_lugar cl ON cl.lugar_id = l.id INNER JOIN "
			+ "categorias c ON cl.categoria_id = c.id WHERE c.sub_id = :name AND l.estatus = 1 " + "HAVING distance_in_km <= :radio_km "
			+ "ORDER BY distance_in_km ASC ", nativeQuery = true)
	List<Place> findByCategory(@Param("name") String name, @Param("mi_latitud") Double latitud,
			@Param("mi_longitud") Double longitud, @Param("radio_km") int radioKm);
	
	@Query(value="SELECT * FROM lugares l WHERE l.estatus = :status_place",nativeQuery = true)
	List<Place> findByStatus(@Param("status_place") String status);
}