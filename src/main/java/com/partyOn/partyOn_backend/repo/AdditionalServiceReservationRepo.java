package com.partyOn.partyOn_backend.repo;

import com.partyOn.partyOn_backend.model.AdditionalServiceReservation;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AdditionalServiceReservationRepo extends JpaRepository<AdditionalServiceReservation,Long> {
	
	@Query(value = "SELECT * FROM reservaciones_servicio_adicional r WHERE r.usuario_id = :id_user AND r.dia_evento >= :now", nativeQuery = true)
	List<AdditionalServiceReservation> findReservationServAdditionalFromNow(@Param("id_user") Long id, @Param("now") String fechaActual);
}
