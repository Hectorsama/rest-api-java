package com.partyOn.partyOn_backend.repo;

import com.partyOn.partyOn_backend.model.Guest;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GuestRepo extends JpaRepository<Guest, Long> {
}
