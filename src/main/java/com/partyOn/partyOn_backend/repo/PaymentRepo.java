package com.partyOn.partyOn_backend.repo;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.partyOn.partyOn_backend.model.Payment;

@Repository
public interface PaymentRepo extends CrudRepository<Payment, Long> {

	@Query(value = "SELECT * FROM pagos p WHERE p.reservacion_id = :id_reservation", nativeQuery = true)
	Payment findByIdReservation(@Param("id_reservation") Long idReservation);

	@Modifying
	@Query(value = "DELETE FROM pagos WHERE reservacion_id = :id_reservation", nativeQuery = true)
	void deleteByIdReservation(@Param("id_reservation") Long idReservation);

}
