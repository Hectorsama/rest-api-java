package com.partyOn.partyOn_backend.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.partyOn.partyOn_backend.model.Events;

@Repository
public interface EventsRepo extends CrudRepository<Events,Long> {

//	@Query("SELECT e FROM Events e, Reservation r "+
//	"WHERE e.reservation = r.id AND r.usuario_id = :id_user")
//	Iterable <Events> findByIdUser(@Param("id_user") Long id);
	
	@Query(value = "SELECT * FROM eventos e WHERE e.reservacion_id = :id_reservacion",nativeQuery = true)
	Events findByIdReservation(@Param("id_reservacion") Long idReservation);
}
