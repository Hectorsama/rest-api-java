package com.partyOn.partyOn_backend.repo;

import com.partyOn.partyOn_backend.model.Services;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;




@Repository
public interface ServiceRepo extends CrudRepository<Services, Long> {

    @Query(value="SELECT * FROM servicios s WHERE s.nombre = :name", nativeQuery = true)
    Services findByNameId(@Param("name") String name);

    @Query(value="SELECT * FROM servicios s WHERE s.sub_id = :sub_id", nativeQuery = true)
    Services findBySubId(@Param("sub_id") String sub_id);

}