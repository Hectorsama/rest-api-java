package com.partyOn.partyOn_backend.repo;

import com.partyOn.partyOn_backend.model.BusyDay;
import com.partyOn.partyOn_backend.model.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.OffsetDateTime;

public interface BusyDayRepository extends CrudRepository<BusyDay, Long> {

    @Query(value="SELECT id FROM dias_ocupados WHERE dia = :dia", nativeQuery = true)
    Long findId(@Param("dia")OffsetDateTime day);
}
