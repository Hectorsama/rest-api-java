package com.partyOn.partyOn_backend.repo;

import java.util.List;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.partyOn.partyOn_backend.model.User;

@Repository
public interface UserRepo extends PagingAndSortingRepository<User, Long> {

    @Query(value ="SELECT * FROM usuarios u where u.uid_firebase = :uid_firebase" , nativeQuery = true)
    User findByUid(@Param("uid_firebase") String uid_firebase);


    @Modifying(clearAutomatically = true)
    @Query(value = "update usuarios u set u.estatus =:status where u.id =:id", nativeQuery = true)
    void updateStatus(@Param("id") Long id, @Param("status") String status);

    @Modifying(clearAutomatically = true)
    @Query(value = "update usuarios u set u.foto_perfil =:foto_perfil where u.id =:id", nativeQuery = true)
    void updateImgSrc(@Param("id") Long id, @Param("foto_perfil") String status);

    @Query(value = "SELECT COUNT(*) FROM favoritos WHERE usuario_id = :id_user AND lugar_id = :id_place", nativeQuery = true)
    int existeFavorito(@Param("id_place")Long idPlace,@Param("id_user")Long idUser);

}
