package com.partyOn.partyOn_backend.repo;
import com.partyOn.partyOn_backend.model.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;


@Repository
public interface CategoryRepo extends CrudRepository<Category, Long> {

    @Query(value="SELECT * FROM categorias c WHERE c.nombre = :name", nativeQuery = true)
    Category findByName(@Param("name") String name);

    @Query(value="SELECT * FROM categorias c WHERE c.sub_id = :name", nativeQuery = true)
    Category findByShortName(@Param("name") String name);
    
    @Query(value="SELECT c.id FROM categorias c WHERE c.nombre = :name", nativeQuery = true)
    Long findByNameId(@Param("name") String name);

    @Query(value="SELECT id FROM categorias WHERE sub_id = :sub_id", nativeQuery = true)
    Long findBySubId(@Param("sub_id") String sub_id);
}