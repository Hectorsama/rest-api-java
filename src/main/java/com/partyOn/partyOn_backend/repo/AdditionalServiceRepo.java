package com.partyOn.partyOn_backend.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.partyOn.partyOn_backend.model.AdditionalService;

public interface AdditionalServiceRepo extends JpaRepository<AdditionalService,Long> {

	
}
