package com.partyOn.partyOn_backend.repo;

import com.partyOn.partyOn_backend.model.AdditionalServiceCategory;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface AdditionalServiceCategoryRepo extends CrudRepository<AdditionalServiceCategory,Long> {

	@Query(value="SELECT * FROM categorias_servicio_adicional t WHERE t.sub_id = :sub_id", nativeQuery = true)
	AdditionalServiceCategory findBySubId(@Param("sub_id") String sub_id);

}
