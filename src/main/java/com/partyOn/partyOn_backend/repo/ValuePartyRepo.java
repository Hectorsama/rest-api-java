package com.partyOn.partyOn_backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.partyOn.partyOn_backend.model.ValueParty;

@Repository
public interface ValuePartyRepo extends JpaRepository<ValueParty, String>{

    @Query(value ="SELECT * FROM party_values v where v.nombre_tabla LIKE :table_name AND v.llave LIKE :key" , nativeQuery = true)
	ValueParty findByTableAndKey(@Param("table_name")String tableName,@Param("key")String key);
}
