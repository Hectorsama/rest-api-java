package com.partyOn.partyOn_backend.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.partyOn.partyOn_backend.model.Valuation;

@Repository
public interface ValuationRepo extends JpaRepository<Valuation, Long> {

}
