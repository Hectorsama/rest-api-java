package com.partyOn.partyOn_backend.repo;

import com.partyOn.partyOn_backend.model.Photos;
import org.springframework.data.repository.CrudRepository;

public interface PhotosRepo extends CrudRepository<Photos,Long> {

}
