package com.partyOn.partyOn_backend.repo;

import com.partyOn.partyOn_backend.model.PaymentPlace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PaymentPlaceRepo extends JpaRepository<PaymentPlace, Long> {

	@Query(value="SELECT * FROM pago_lugar p WHERE p.lugar_id = :id_place",nativeQuery = true)
	PaymentPlace getByIdPlace(@Param("id_place") Long idPlace);
}
