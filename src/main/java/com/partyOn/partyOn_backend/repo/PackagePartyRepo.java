package com.partyOn.partyOn_backend.repo;

import com.partyOn.partyOn_backend.model.Services;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.partyOn.partyOn_backend.model.PackageParty;

@Repository
public interface PackagePartyRepo extends JpaRepository<PackageParty, Long> {
    @Query(value = "SELECT * FROM paquetes p WHERE p.nombre = :nombre", nativeQuery = true)
    PackageParty findByName(@Param("nombre") String nombre);
}
