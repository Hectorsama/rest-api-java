package com.partyOn.partyOn_backend.repo;

import com.partyOn.partyOn_backend.model.User;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.partyOn.partyOn_backend.model.Guest;
import com.partyOn.partyOn_backend.model.Reservation;

@Repository
public interface ReservationRepo extends CrudRepository<Reservation, Long> {

	@Modifying(clearAutomatically = true)
	@Query(value = "update reservaciones r set r.estatus =:status, r.costo_total=:cost, r.cantidad_horas=:hour where r.id =:id", nativeQuery = true)
	void update(@Param("id") Long id, @Param("status") String status, @Param("cost") BigDecimal cost, @Param("hour") int hour);

	@Modifying(clearAutomatically = true)
	@Query(value = "update reservaciones r set r.estatus =:status where r.id =:id", nativeQuery = true)
	void updateStatus(@Param("id") Long id, @Param("status") String status);

	@Modifying(clearAutomatically = true)
	@Query(value = "update reservaciones r set r.cargo_partyon =:cargo_partyon, r.cargo_recinto =:cargo_recinto where r.id =:id", nativeQuery = true)
	void updateStatusCharges(@Param("id") Long id, @Param("cargo_partyon") BigDecimal cargo_partyon,@Param("cargo_recinto") BigDecimal cargo_recinto);

	@Query(value = "SELECT * FROM reservaciones r WHERE r.usuario_id = :id_user AND r.dia_evento >= :now", nativeQuery = true)
	List<Reservation> findReservationFromNow(@Param("id_user") Long id, @Param("now") String fechaActual);

	@Query(value = "SELECT * FROM reservaciones r INNER JOIN eventos e ON e.reservacion_id = r.id INNER JOIN invitados i ON e.id = i.evento_id "
			+ "WHERE i.usuario_id = :id_user AND r.dia_evento >= :now", nativeQuery = true)
	List<Reservation> findInvitationFromNow(@Param("id_user") Long id, @Param("now") String fechaActual); // Corregir

	@Query(value = "SELECT * FROM reservaciones r WHERE r.usuario_id = :id_user AND r.dia_evento LIKE :date%", nativeQuery = true)
	List<Reservation> findReservationsFromDate(@Param("id_user") Long id, @Param("date") String fecha);

	@Query(value = "SELECT * FROM reservaciones r INNER JOIN eventos e ON e.reservacion_id = r.id INNER JOIN invitados i ON e.id = i.evento_id "
			+ "WHERE i.usuario_id = :id_user AND r.dia_evento LIKE :date%", nativeQuery = true)
	List<Reservation> findInvitacionesFromDate(@Param("id_user") Long id, @Param("date") String fecha);

	@Query(value = "SELECT * FROM reservaciones r WHERE r.estatus = :status ", nativeQuery = true)
	List<Reservation> findReservationStatusTicketGenereted(@Param("status") String status);

}
