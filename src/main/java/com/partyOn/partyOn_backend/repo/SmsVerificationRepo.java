package com.partyOn.partyOn_backend.repo;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.partyOn.partyOn_backend.model.SmsVerification;

import java.time.OffsetDateTime;

@Repository
public interface SmsVerificationRepo extends CrudRepository<SmsVerification, Long> {

    @Query(value="SELECT codigo FROM verificacion_sms WHERE usuario_id = :id", nativeQuery = true)
    String findCode(@Param("id") Long id);

    @Query(value="SELECT telefono FROM usuarios WHERE telefono = :number", nativeQuery = true)
    String findNumber(@Param("number") String number);

    @Modifying
    @Query(value="DELETE FROM verificacion_sms WHERE usuario_id = :id_usuario",nativeQuery = true)
    void deleteByIdUSer(@Param("id_usuario") Long id);
}
