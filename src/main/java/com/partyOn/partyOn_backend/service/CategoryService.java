package com.partyOn.partyOn_backend.service;

import com.partyOn.partyOn_backend.model.Category;
import com.partyOn.partyOn_backend.repo.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryService {
    @Autowired
    private CategoryRepo categoryRepo;

    public void insert(Category category) {
        this.categoryRepo.save(category);
    }

    public Iterable<Category> getAll() {
        return this.categoryRepo.findAll();
    }

    public Category getById(Long id) {
        return this.categoryRepo.findById(id).orElse(null);
    }
}

