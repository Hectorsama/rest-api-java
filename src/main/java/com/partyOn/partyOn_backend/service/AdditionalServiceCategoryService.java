package com.partyOn.partyOn_backend.service;

import com.partyOn.partyOn_backend.model.AdditionalService;
import com.partyOn.partyOn_backend.model.Category;

import com.partyOn.partyOn_backend.model.AdditionalServiceCategory;
import com.partyOn.partyOn_backend.repo.AdditionalServiceCategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdditionalServiceCategoryService {

    @Autowired
    private AdditionalServiceCategoryRepo  additionalServiceCategoryRepo;

    public Iterable<AdditionalServiceCategory> getAll() {
        return this.additionalServiceCategoryRepo.findAll();
    }

    public AdditionalServiceCategory findSubId(String subId) {
        return this.additionalServiceCategoryRepo.findBySubId(subId);
    }


}
