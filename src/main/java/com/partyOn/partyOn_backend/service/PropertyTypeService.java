package com.partyOn.partyOn_backend.service;

import com.partyOn.partyOn_backend.model.Category;

import com.partyOn.partyOn_backend.model.PropertyType;
import com.partyOn.partyOn_backend.model.Services;
import com.partyOn.partyOn_backend.repo.PropertyTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PropertyTypeService {
    @Autowired
    private PropertyTypeRepo propertyTypeRepo;

    public void insert(PropertyType category) {
        this.propertyTypeRepo.save(category);
    }

    public Iterable<PropertyType> getAll() {
        return this.propertyTypeRepo.findAll();
    }

    public PropertyType getById(Long id) {
        return this.propertyTypeRepo.findById(id).orElse(null);
    }

    public Long findByNameIdP(PropertyType propertyType) {
        return this.propertyTypeRepo.findByTypeId(propertyType.getType()).getId();
    }

    public Long findBySubIdP(PropertyType propertyType) {
        return this.propertyTypeRepo.findBySubId(propertyType.getSubId()).getId();
    }
}
