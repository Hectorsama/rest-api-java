package com.partyOn.partyOn_backend.service;

import javax.transaction.Transactional;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.partyOn.partyOn_backend.model.SmsVerification;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.repo.SmsVerificationRepo;
import com.partyOn.partyOn_backend.repo.UserRepo;

@Service
public class SmsVerificationService {

	@Autowired
	private SmsVerificationRepo smsVerificationRepo;

	@Autowired
	private Environment env;

	@Autowired
	private UserRepo userRepo;

	/**
	 * Genera el codigo que se usara para verificar el numero telefonico, ademas
	 * tambien se envia mensaje al telefono del usuario dado
	 *
	 * @param user      Usuario al que se encuentra ligado ese codigo de
	 *                  verificacion
	 * @param numeroTel
	 */
	@Transactional
	public void generaCodigo(User user, String numeroTel) {
		final String ACCOUNT_SID = env.getProperty("ACCOUNT_SID");
		final String AUTH_TOKEN = env.getProperty("AUTH_TOKEN");

		// user.setPhone(numeroTel);

		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
		SmsVerification smsVerification = new SmsVerification();
		int codigo = codeFactory();

		try {
			if (user.findVerificationCode() != null) {
				System.out.println("El usuario tenia codigo pendiente,se actualiza");
				
				user.findVerificationCode().setCode(codigo);
			} else {
				System.out.println("El usuario no tenia codigo pendiente");
				smsVerification.setCode(codigo);
				smsVerification.setUser(user);
//		Message message = Message.creator(new PhoneNumber(numeroTel), // to
//				new PhoneNumber("+14437752210"), // from
//				String.format("El código de verificación es %s", codigo)).create();

				smsVerificationRepo.save(smsVerification);
			}
		} catch (DataIntegrityViolationException e) {
			System.out.println("number already exist");
		}
	}

	public int codeFactory() {
		int valorMin = 10000;
		int valorMax = 100000;
		Integer valorEntero = (int) (Math.random() * (valorMax - valorMin + 1) + valorMin);

		return valorEntero;
	}

	@Transactional
	public String getCode(Long idUser) {
		return smsVerificationRepo.findCode(idUser);
	}

	@Transactional
	public void deleteCode(User user) {
		this.smsVerificationRepo.deleteByIdUSer(user.getId());
	}
}
