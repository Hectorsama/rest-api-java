package com.partyOn.partyOn_backend.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.partyOn.partyOn_backend.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.partyOn.partyOn_backend.model.AdditionalService;
import com.partyOn.partyOn_backend.repo.EventsRepo;
import com.partyOn.partyOn_backend.repo.AdditionalServiceRepo;
import com.partyOn.partyOn_backend.repo.PlacesRepo;
import com.partyOn.partyOn_backend.repo.UserRepo;

@Service
public class EventService {

	@Autowired
	private EventsRepo eventRepo;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private PlacesRepo placeRepo;
	
	@Autowired
	AdditionalServiceRepo extraRepo;
	
	public Iterable<Events> all() {
		return this.eventRepo.findAll();
	}

	public List<Events> fromUser(Long id) {
		User user = this.userRepo.findById(id).orElse(null);
		List<Events> res = new ArrayList<>();

		for (Reservation r : user.getReservations()) {
			Events eventoDeReservacion=eventRepo.findByIdReservation(r.getId());
			if (eventoDeReservacion != null) {
				res.add(eventoDeReservacion);
			}
		}
		return res;
	}

	public List<Events> fromPlace(Long id) {
		Place place= this.placeRepo.findById(id).orElse(null);
		List<Reservation> reservaciones= place.allReservations();
		List<Events> eventosDeLugar = new ArrayList();
		for(Reservation r: reservaciones) {
			Events eventoDeReservacion=eventRepo.findByIdReservation(r.getId());
			if(eventoDeReservacion!=null) {
				eventosDeLugar.add(eventoDeReservacion);
			}
		}
		
		return eventosDeLugar;
	}
	
	@Transactional
	public void addExtra(Long idEvent,Long idExtra) {
		Events event= this.eventRepo.findById(idEvent).orElse(null);
		AdditionalService extra= this.extraRepo.findById(idExtra).orElse(null);
		
		event.addAdditionalService(extra);
	}
	
	@Transactional
	public void deleteExtra(Long idEvent, long idExtra) {
		Events event= this.eventRepo.findById(idEvent).orElse(null);
		AdditionalService extra= this.extraRepo.findById(idExtra).orElse(null);
		
		extra.removeEvent(event);
		event.removeAdditionalService(extra);
	}
}
