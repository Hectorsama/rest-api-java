package com.partyOn.partyOn_backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.partyOn.partyOn_backend.model.Provider;
import com.partyOn.partyOn_backend.repo.ProviderRepo;

@Service
public class ProviderService {
	@Autowired
	private ProviderRepo providerRepo;
	
	public Iterable<Provider> all(){
		return this.providerRepo.findAll();
	}
}
