package com.partyOn.partyOn_backend.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.partyOn.partyOn_backend.model.Events;
import com.partyOn.partyOn_backend.model.Place;
import com.partyOn.partyOn_backend.model.Valuation;
import com.partyOn.partyOn_backend.repo.EventsRepo;
import com.partyOn.partyOn_backend.repo.PlacesRepo;
import com.partyOn.partyOn_backend.repo.ValuationRepo;

@Service
public class ValuationService {

	@Autowired
	private ValuationRepo valuationRepo;

	@Autowired
	private PlacesRepo placeRepo;

	@Autowired
	private EventsRepo eventRepo;

	@Transactional
	public void addValuation(Long idPlace, Long idEvent, int rating) {
		Place place = placeRepo.findById(idPlace).orElse(null);
		Events event = eventRepo.findById(idEvent).orElse(null);

		Valuation valuation = new Valuation();
		valuation.setPlace(place);
		valuation.setEvent(event);
		valuation.setRating(rating);
		valuationRepo.save(valuation);
	}
}
