package com.partyOn.partyOn_backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import com.partyOn.partyOn_backend.model.Place;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.repo.PlacesRepo;
import com.partyOn.partyOn_backend.repo.UserRepo;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Service
public class FavoriteService {

	private static final Log log = LogFactory.getLog(FavoriteService.class);
	@Autowired
	EntityManager entityManager;
	@Autowired
	private UserRepo userRepo;

	@Autowired
	private PlacesRepo placeRepo;

	@Transactional
	public void addFavorite(Long idPlace, Long IdUser) {
		User user = this.userRepo.findById(IdUser).orElse(null);
		Place nwFavorite = this.placeRepo.findById(idPlace).orElse(null);
		log.info("Lugar encontrado: " + nwFavorite.getName());
		user.addLike(nwFavorite);

	}

	public List<Place> favoritos(Long id) {
		User user = this.userRepo.findById(id).orElse(null);
		return user.getLikes();
	}

	@Transactional
	public void dislike(Long idPlace, Long idUser) {
		User user = this.userRepo.findById(idUser).orElse(null);
		Place place = this.placeRepo.findById(idPlace).orElse(null);
		log.info("size: " + user.getLikes().size());
		user.dislike(place);
		log.info("size: " + user.getLikes().size());
		place.deleteFavorito(user);
	}

}
