package com.partyOn.partyOn_backend.service;
import com.partyOn.partyOn_backend.model.*;
import com.partyOn.partyOn_backend.repo.*;
import com.partyOn.partyOn_backend.util.ConverterDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BusyDayService {
    @Autowired
    private BusyDayRepository busyDayRepository;
    @Autowired
    private PlacesRepo placesRepo;
    public void insert(BusyDay busyDay, Long id) {
        Place place = this.placesRepo.findById(id).orElse(null);
        busyDay.setPlace(place);
        this.busyDayRepository.save(busyDay);
    }
    public Iterable<BusyDay> getAll() {
        return this.busyDayRepository.findAll();
    }

    public BusyDay getById(Long id) {
        return this.busyDayRepository.findById(id).orElse(null);
    }
    
    
    public Iterable<BusyDay> getBusyDaysFromPlace(Long id){
    	Place place= this.placesRepo.findById(id).orElse(null);
    	List<BusyDay> diasConTiempoLocal=new ArrayList<>();
    	String zonaHoraria=place.getTimeZone();
    	
    	
    	for (BusyDay busyDay : place.allBusyDays()) {
    		//System.out.println(busyDay.getDate());
			BusyDay tempBusyDay = busyDay;
			tempBusyDay.setDate(ConverterDate.toZoneIdFromUTC(busyDay.getDate(), zonaHoraria));
			diasConTiempoLocal.add(tempBusyDay);
		}
    	
    	return  diasConTiempoLocal;
    }
}

