package com.partyOn.partyOn_backend.service;


import com.partyOn.partyOn_backend.mail.MailService;
import com.partyOn.partyOn_backend.mail.Mails;
import com.partyOn.partyOn_backend.model.BusyDay;
import com.partyOn.partyOn_backend.model.Payment;
import com.partyOn.partyOn_backend.model.Place;
import com.partyOn.partyOn_backend.model.Reservation;
import com.partyOn.partyOn_backend.repo.ReservationRepo;
import com.partyOn.partyOn_backend.util.ConverterDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Clock;
import java.time.OffsetDateTime;
import java.time.format.TextStyle;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


@Service
public class TransferService {
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private PaymentService paymentService;

    @Autowired
    private ReservationRepo reservationRepo;

    @Autowired
    MailService mailService;
    @Autowired
    private Environment env;


    final String ESTATUS = "3";
    final String CARGOPARTYON = ".05";


    @Transactional
    public void insertPayment(Long idReservation, Long idUser) {
        Reservation reservation = this.reservationRepo.findById(idReservation).orElse(null);
        String orderId = "oid-" + idReservation.toString();
        Payment payment = new Payment();
        payment.setOrderNumber(orderId);
        payment.setReservation(reservation);
        OffsetDateTime offsetdatetime
                = OffsetDateTime.now(
                Clock.systemUTC());
        payment.setPayDay(offsetdatetime);
        this.paymentService.add(payment, idUser);

    }

    @Transactional
    public void updateStatusTicket(Long idReservation, BigDecimal cost, int hour) throws Exception {
        Reservation reservation = this.reservationRepo.findById(idReservation).orElse(null);

        BigDecimal cargoPartyOn = cost.multiply(new BigDecimal(CARGOPARTYON)).setScale(2, RoundingMode.CEILING);
        BigDecimal percentage = new BigDecimal(reservation.getRequestedPlace().getPercentage()).setScale(2, RoundingMode.CEILING);
        BigDecimal cargoApartado = cost.multiply(percentage).setScale(2, RoundingMode.CEILING);

        /*Agregamos dia ocupado*/
        Place place = reservation.getRequestedPlace();
        BusyDay busyDay = new BusyDay();
        busyDay.setPlace(place);
        busyDay.setDate(reservation.getEventDay());
        place.insertBusyDay(busyDay);


        this.reservationRepo.update(idReservation, ESTATUS, cost, hour);
        this.reservationService.updateReservationFirebase(idReservation, ESTATUS);
        this.reservationRepo.updateStatusCharges(idReservation, cargoPartyOn, cargoApartado);
        String orderId = "oid-" + idReservation.toString();
        Payment payment = new Payment();
        payment.setOrderNumber(orderId);
        payment.setReservation(reservation);
        OffsetDateTime offsetdatetime
                = OffsetDateTime.now(
                Clock.systemUTC());
        payment.setPayDay(offsetdatetime);
        this.paymentService.add(payment, reservation.findApplicant().getId());

        String zonaHoraria = reservation.getRequestedPlace().getTimeZone();
        OffsetDateTime diaEventoConTiempoLocal = ConverterDate.toZoneIdFromUTC(reservation.getEventDay(), zonaHoraria);
        int year_int = diaEventoConTiempoLocal.getYear();
        String year = Integer.toString(year_int).replace(",", "");
        String month = reservation.getEventDay().toLocalDate().getMonth().getDisplayName(TextStyle.FULL, new Locale("es", "ES"));
        System.out.println("--------->" + month);
        String mailApplicant = reservation.findApplicant().getEmail();
        String mailOwner = reservation.getRequestedPlace().findUser().getEmail();
        String mail_env = env.getProperty("MAIL_PARTYON");
        String nameApplicant = reservation.findApplicant().getName();

        /**Mail to applicant*/
        Mails mail = new Mails();
        mail.setMailFrom(mail_env);
        mail.setMailTo(mailApplicant);
        mail.setMailSubject("Reservación de un recinto");

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("dia", reservation.getEventDay().getDayOfMonth());
        model.put("mes", month);
        model.put("year", year);
        model.put("date", reservation.getEventDay());
        model.put("name", reservation.getRequestedPlace().findUser().getName());
        model.put("name_place", reservation.getRequestedPlace().getName());
        model.put("price", cost);
        model.put("pricePartyOn", cargoPartyOn.toString());
        model.put("priceReservation", cargoApartado.toString());
        model.put("mail", mailOwner);
        mail.setModel(model);
        mailService.sendEmailPlaceUser(mail);

        /**Mail to Owner*/
        Mails mail_owner = new Mails();
        mail_owner.setMailFrom(mail_env);
        mail_owner.setMailTo(mailOwner);
        mail_owner.setMailSubject("Reservación de un recinto");

        Map<String, Object> model_owner = new HashMap<String, Object>();
        model_owner.put("dia", diaEventoConTiempoLocal.getDayOfMonth());
        model_owner.put("mes", month);
        model_owner.put("year", year);
        model_owner.put("date", reservation.getEventDay());
        model_owner.put("nameApplicant", nameApplicant);
        model_owner.put("name_place", reservation.getRequestedPlace().getName());
        model_owner.put("price", cost);
        model_owner.put("pricePartyOn", cargoPartyOn.toString());
        model_owner.put("priceReservation", cargoApartado.toString());
        model_owner.put("mail", mailApplicant);
        mail_owner.setModel(model_owner);
        mailService.sendEmailPlaceOwner(mail_owner);

    }

    /**
     * Pago del cargo de recinto con el porcentaje que determina el propietario del lugar más el cargo de partyOn
     */
    @Transactional
    public void payReservation(Long idReservation) {
        Reservation reservation = this.reservationRepo.findById(idReservation).orElse(null);

        this.reservationService.payReservation(idReservation, reservation.findApplicant());
        String mailApplicant = reservation.findApplicant().getEmail();
        String mailOwner = reservation.getRequestedPlace().findUser().getEmail();

    }


}
