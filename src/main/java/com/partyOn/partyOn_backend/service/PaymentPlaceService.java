package com.partyOn.partyOn_backend.service;

import com.partyOn.partyOn_backend.model.Payment;
import com.partyOn.partyOn_backend.model.PaymentPlace;
import com.partyOn.partyOn_backend.model.Place;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.repo.PaymentPlaceRepo;
import com.partyOn.partyOn_backend.repo.PaymentRepo;
import com.partyOn.partyOn_backend.repo.PlacesRepo;
import com.partyOn.partyOn_backend.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.OffsetDateTime;
@Service
public class PaymentPlaceService {
    @Autowired
    private PaymentPlaceRepo paymentRepo;
    @Autowired
    private PlacesRepo placesRepo;

    public void add(Long id) {
       Place place = this.placesRepo.findById(id).orElse(null);
       PaymentPlace paymentPlace = new PaymentPlace();
       paymentPlace.setOrderNumber("ord-place-"+place.getId());
        OffsetDateTime offsetdatetime
                = OffsetDateTime.now(
                Clock.systemUTC());
        paymentPlace.setPayDay(offsetdatetime);
        paymentPlace.setPlace(place);
        this.paymentRepo.save(paymentPlace);
    }
    
    
    public PaymentPlace getByPlace(Long idPlace) {
    	return this.paymentRepo.getByIdPlace(idPlace);
    }
}
