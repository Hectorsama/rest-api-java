package com.partyOn.partyOn_backend.service;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.partyOn.partyOn_backend.model.Payment;
import com.partyOn.partyOn_backend.model.Place;
import com.partyOn.partyOn_backend.model.Reservation;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.model.ValueParty;
import com.partyOn.partyOn_backend.repo.PlacesRepo;
import com.partyOn.partyOn_backend.repo.UserRepo;
import com.partyOn.partyOn_backend.repo.ValuePartyRepo;

@Service
public class UserService {

	@Autowired
	private UserRepo userRepoitory;

	@Autowired
	private PlacesRepo placesRepo;

	@Autowired
	private ValuePartyRepo valuePartyRepo;
	
	@Transactional
	public void nuevoUsuario(User user) {
		ValueParty valueParty= this.valuePartyRepo.findByTableAndKey("usuarios", "confirmacion_telefono");
		user.setStatus(valueParty.getValue());
		user.setPhone(null);
//		user.setStatus(valueParty.getValue());
		this.userRepoitory.save(user);
	}

	public Iterable<User> users() {
		return this.userRepoitory.findAll();
	}

	/**
	 * @param id
	 * @return user sin las tarjetas, likes y reservaciones
	 */
	public User getById(Long id) {
		User user = this.userRepoitory.findById(id).orElse(null);
		ArrayList<Payment> vacia = new ArrayList<Payment>();
		ArrayList<Place> sinLugares = new ArrayList<Place>();
		ArrayList<Reservation> sinReservaciones = new ArrayList<Reservation>();
		user.setPayments(vacia);
		user.setLikes(sinLugares);
		user.setReservations(sinReservaciones);

		return user;
	}


	public void deleteUser(Long id) {
		this.userRepoitory.deleteById(id);
	}

	public User findByUidFirebase(String uid) {
		User user = this.userRepoitory.findByUid(uid);
		return user;
	}

	public User propietario(Long idPlace) {
		Place place = this.placesRepo.findById(idPlace).orElse(null);
		User user = place.findUser();
		user.setPayments(new ArrayList<>());
		user.setLikes(new ArrayList<>());
		user.setReservations(new ArrayList<>());
		return user;
	}

	@Transactional
	public void uptateStatus(Long id) {
		ValueParty valueParty = this.valuePartyRepo.findByTableAndKey("usuarios", "activo");
		this.userRepoitory.updateStatus(id,valueParty.getValue());
	}

	@Transactional
	public void updateImageProfile(Long id, String url) {
		this.userRepoitory.updateImgSrc(id,url);
	}

}
