package com.partyOn.partyOn_backend.service;

import com.partyOn.partyOn_backend.model.Services;
import com.partyOn.partyOn_backend.repo.ServiceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ServiceService {
    @Autowired
    private ServiceRepo serviceRepo;

    public void insert(Services service) {
        this.serviceRepo.save(service);
    }

    public Iterable<Services> getAll() {
        return this.serviceRepo.findAll();
    }

    public Services getById(Long id) {
        return this.serviceRepo.findById(id).orElse(null);
    }
}