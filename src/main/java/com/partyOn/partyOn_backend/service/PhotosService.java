package com.partyOn.partyOn_backend.service;


import com.partyOn.partyOn_backend.model.Photos;
import com.partyOn.partyOn_backend.model.Place;
import com.partyOn.partyOn_backend.repo.PhotosRepo;
import com.partyOn.partyOn_backend.repo.PlacesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhotosService {
    @Autowired
    private PhotosRepo photosRepo;

    @Autowired
    private PlacesRepo placesRepo;

    public void insert(Photos photo, Long id) {
        Place place = this.placesRepo.findById(id).orElse(null);
        photo.setPlace(place);
        this.photosRepo.save(photo);
    }
}
