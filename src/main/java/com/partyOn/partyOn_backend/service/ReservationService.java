package com.partyOn.partyOn_backend.service;

import java.math.BigDecimal;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import com.google.firebase.FirebaseException;
import com.partyOn.partyOn_backend.jsonAdapter.ReservacionConsultaWrapper;
import com.partyOn.partyOn_backend.jsonAdapter.ReservationWithEvent;
import com.partyOn.partyOn_backend.jsonAdapter.ReservationWithHostWrapper;
import com.partyOn.partyOn_backend.mail.MailService;
import com.partyOn.partyOn_backend.mail.Mails;
import com.partyOn.partyOn_backend.model.*;
import com.partyOn.partyOn_backend.model.firebase.ReservacionWrapper;
import com.partyOn.partyOn_backend.repo.*;
import com.partyOn.partyOn_backend.util.ConverterDate;
import com.twilio.jwt.accesstoken.ChatGrant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DatabaseReference.CompletionListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.partyOn.partyOn_backend.model.firebase.Mensaje;
import com.partyOn.partyOn_backend.model.firebase.Notificacion;
import com.partyOn.partyOn_backend.model.firebase.ReservacionNotificacion;

@Service
public class ReservationService {
    @Autowired
    private ReservationRepo reservationRepo;

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private PlacesRepo placeRepo;
    @Autowired
    private EventsRepo eventRepo;
    @Autowired
    private BusyDayRepository BusyDayRepo;
    @Autowired
    private BusyDayRepository busyDayRepository;

    @Autowired
    private ValuePartyRepo valuePartyRepo;

    @Autowired
    private PaymentRepo paymentRepo;

    @Autowired
    private AdditionalServiceReservationRepo additionalServiceReservationRepo;
    @Autowired
    MailService mailService;
    @Autowired
    private Environment env;

    private String tokenPropietario = "";
    private String tokenUsuarioQueSolicita = "";

    /**
     * La fecha de reservacion se recibira en UTC asi como la fecha del evento
     *
     * @param reservation
     * @param idApplicant
     * @param idPlace
     * @return el id de la reservacion generada
     */
    @Transactional
    public Long nuevaReservacion(Reservation reservation, Long idApplicant, Long idPlace) {
        User user = this.userRepo.findById(idApplicant).orElse(null);
        Place requestedPlace = this.placeRepo.findById(idPlace).orElse(null);
        ValueParty value = this.valuePartyRepo.findByTableAndKey("reservaciones", "pendiente");
        reservation.setStatus(value.getValue());
        reservation.setApplicant(user);
        reservation.setRequestedPlace(requestedPlace);

        Long idNewReservacion = this.reservationRepo.save(reservation).getId();
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        /*
         * La fecha del evento llega en formato UTC -> Se convierte a la zona horaria del lugar solo extrayendo anio,mes,dia etc.
         * de la fecha en UTC -> Se regresa esa fecha en esa zona horaria del lugar a UTC
         *
         */
        OffsetDateTime nwEventDay = ConverterDate.toZoneIdInUTC(reservation.getEventDay(),
                requestedPlace.getTimeZone());
        reservation.setEventDay(nwEventDay);

        /*
         * Guardamos el valor de la fecha que se realizó tomando como referencia la zona horaria del lugar
         */
        OffsetDateTime nwReservationDate = ConverterDate.toZoneIdFromUTC(reservation.getReservationDay(),
                requestedPlace.getTimeZone());
        reservation.setReservationDay(nwReservationDate);
        System.out.println("fecha de reservacion: " + nwReservationDate);


        /**
         * Calculamos el que tipo de reservación será
         */
        Long numberDay = setTypeReservation(reservation.getEventDay());

        System.out.println("ENTRO A RESEVACION Y CALCULARA LOS DIAS QUE FALTAN PARA EL ENVENTO------>" + numberDay);
        reservation.setReservationType(numberDay.intValue());

        /**
         * Creamos las referencias a la colecciones de Firebase
         */
        DatabaseReference databaseReference = database.getReference("Chat/" + idNewReservacion);
        DatabaseReference databaseReferenceReservation = database.getReference("Reservation");
        /**
         * Creacion de la sala de chat al reservar
         */
//        String fecha = OffsetDateTime.now().format(DateTimeFormatter.ofPattern("d/MMMM/y"));
        String message;
        message = reservation.getNote();
        if (reservation.getNote().equals(null) || reservation.getNote().trim().isEmpty()) {
            message = "Hola estoy interesado en reservar este recinto";
        }
        Mensaje firstMensaje = new Mensaje();
        firstMensaje.setDate(nwReservationDate.toString());
        firstMensaje.setIdApplicant(user.getUidFirebase());
        firstMensaje.setIdOwner(requestedPlace.findUser().getUidFirebase());
        firstMensaje.setMsg(message);
        firstMensaje.setKeyEmisor(user.getUidFirebase());
        // (message, user.getName(), user.getUidFirebase(), "", "", fecha);

        databaseReference.push().setValue(firstMensaje, new CompletionListener() {

            @Override
            public void onComplete(DatabaseError error, DatabaseReference ref) {
                System.out.println("Se generó sala de chat");

            }
        });

        /**
         * Creacion del documento Reservacion
         */

        ReservacionWrapper reservationModelFirebase = new ReservacionWrapper();
        OffsetDateTime eventDayLocal = ConverterDate.toZoneIdFromUTC(nwEventDay, requestedPlace.getTimeZone());

        reservationModelFirebase.setIdReservation(idNewReservacion);
        reservationModelFirebase.setClientID(user.getUidFirebase());
        reservationModelFirebase.setOwnerID(reservation.getRequestedPlace().findUser().getUidFirebase());
        reservationModelFirebase.setPlaceID(reservation.getRequestedPlace().getId().intValue());
        reservationModelFirebase.setNamePlace(reservation.getRequestedPlace().getName());
        reservationModelFirebase.setImgURLPlace(reservation.getRequestedPlace().getPhotos().get(0).getImgSrc());
        reservationModelFirebase.setKeyEmisor(user.getUidFirebase());
        reservationModelFirebase.setLastMessage(message);
        reservationModelFirebase.setIsRead(0);
        reservationModelFirebase.setStatus(reservation.getStatus());
        reservationModelFirebase.setReservationDay(reservation.getReservationDay().toString());
        reservationModelFirebase.setEventDay(eventDayLocal.toString());
        reservationModelFirebase.setNumberHours(reservation.getNumberHours());
        reservationModelFirebase.setSendTime(reservation.getReservationDay().toString());
        reservationModelFirebase.setTotalCost(reservation.getTotalCost().toString());

        databaseReferenceReservation.child(idNewReservacion.toString()).setValue(reservationModelFirebase,
                new CompletionListener() {

                    @Override
                    public void onComplete(DatabaseError error, DatabaseReference ref) {
                        System.out.println("Se generó sala de chat");
                    }
                });

        return idNewReservacion;

    }


    public Iterable<Reservation> allReservations() {
        return this.reservationRepo.findAll();
    }

    public Iterable<Reservation> reservationsFromUser(Long id) {
        User user = userRepo.findById(id).orElse(null);
        Iterable<Reservation> allReservation = user.getReservations();

        for (Reservation reservation : allReservation) {
            OffsetDateTime eventDay = ConverterDate.toZoneIdFromUTC(reservation.getEventDay(),
                    reservation.getRequestedPlace().getTimeZone());
            reservation.setEventDay(eventDay);
        }
        return allReservation;
    }

    public List<Reservation> reservationsFromPlace(Long idUser) {
        List<Place> misLugares = this.placeRepo.findByIdPropietario(idUser);
        System.out.println("Se encontraron " + misLugares.size() + "lugares");
        List<Reservation> reservaciones = new ArrayList<>();

        for (Place p : misLugares) {
            reservaciones = Stream.concat(reservaciones.stream(), p.allReservations().stream())
                    .collect(Collectors.toList());

            for (Reservation reservation : reservaciones) {
                OffsetDateTime eventDay = ConverterDate.toZoneIdFromUTC(reservation.getEventDay(), p.getTimeZone());
                reservation.setEventDay(eventDay);
            }
        }

        return reservaciones;
    }

    @Transactional
    public void confirmReservation(Long idReservation) {
        Reservation reservation = this.reservationRepo.findById(idReservation).orElse(null);
        ValueParty value = this.valuePartyRepo.findByTableAndKey("reservaciones", "confirmado");
        reservation.setStatus(value.getValue());
    }

    public User getApplicant(Long idReservation) {
        Reservation reservation = this.reservationRepo.findById(idReservation).orElse(null);
        return reservation.findApplicant();
    }

    @Transactional
    public void payReservation(Long idReservation, User user) {
        Reservation reservation = this.reservationRepo.findById(idReservation).orElse(null);

        ValueParty value = this.valuePartyRepo.findByTableAndKey("reservaciones", "pago_usuario");
        reservation.setStatus(value.getValue());

        Events myEvent = new Events();
        myEvent.setName("Fiesta de " + user.getName());
        myEvent.setDescription("Esta fiesta es organizada por " + user.getName() + ". No Faltes!!!");
        myEvent.setReservation(reservation);

        this.eventRepo.save(myEvent);
    }

    @Transactional
    public void updateReservation(Long idReservation, String status, BigDecimal cost, int hour) {
        this.reservationRepo.update(idReservation, status, cost, hour);
        Reservation reservation = this.reservationRepo.findById(idReservation).orElse(null);
        Place place = reservation.getRequestedPlace();
        BusyDay busyDay = new BusyDay();
        busyDay.setPlace(place);
        busyDay.setDate(reservation.getEventDay());
        place.insertBusyDay(busyDay);
    }

    @Transactional
    public void updateStatusReservation(Long idReservation, String status) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        Reservation reservation = this.reservationRepo.findById(idReservation).orElse(null);
        String uidSolicitante = reservation.findApplicant().getUidFirebase();
        this.reservationRepo.updateStatus(idReservation, status); // Actualiza en BD

    }

    @Transactional
    public void cancelReservation(Long idReservation) throws Exception {
        String ESTATUS = "4";
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        Reservation reservation = this.reservationRepo.findById(idReservation).orElse(null);
        String uidSolicitante = reservation.findApplicant().getUidFirebase();
        System.out.println(reservation.getId().toString());
        Long idBusyDay = this.busyDayRepository.findId(reservation.getEventDay());
        if (reservation.getStatus().equals("3")) {
            this.busyDayRepository.deleteById(idBusyDay);
            this.paymentRepo.deleteByIdReservation(reservation.getId());
        }

        updateReservationFirebase(idReservation, "4");

        /** Eliminamos de la base de datos de firebase */
        database.getReference("Reservation").child(idReservation.toString()).removeValue(null);
        database.getReference("Chat").child(idReservation.toString()).removeValue(null);

        /**
         * Emails*/
        String mailApplicant = reservation.findApplicant().getEmail();
        String mailOwner = reservation.getRequestedPlace().findUser().getEmail();
        ArrayList<String> mails = new ArrayList<>();

        mails.add(mailApplicant);
        mails.add(mailOwner);
        /**Mail to applicant*/

        for (String email : mails) {
            String mail_env = env.getProperty("MAIL_PARTYON");
            Mails mail = new Mails();
            mail.setMailFrom(mail_env);
            mail.setMailTo(email);
            mail.setMailSubject("Cancelación de un recinto");
            OffsetDateTime date = OffsetDateTime.now();
            int year_int = date.getYear();
            String year = Integer.toString(year_int).replace(",", "");
            String month = date.toLocalDate().getMonth().getDisplayName(TextStyle.FULL, new Locale("es", "ES"));
            Map<String, Object> model = new HashMap<String, Object>();
            model.put("month", month);
            model.put("year", year);
            model.put("day", date.getDayOfMonth());
            model.put("name_place", reservation.getRequestedPlace().getName());
            model.put("order", "oid-" + reservation.getId());
            model.put("mail", mailOwner);
            mail.setModel(model);
            mailService.sendEmailCancelRervation(mail);
        }


//        String leyendaEstatus = "Lo sentimos: se ha cancelado tu reservación";
//        ReservacionNotificacion notificacion = new ReservacionNotificacion(idReservation);
//        notificacion.setTitle(reservation.getRequestedPlace().getName());
//        notificacion.setBody(leyendaEstatus);
//        notificacion.setPhotoApplicant("");
//        notificacion.setTipoNotificacion(Notificacion.ESTATUS_ACTUALIZADO_DE_RESERVACION);
//
//        DatabaseReference ref = database.getReference().child("usuarios").child(uidSolicitante);
//        Query query = ref.orderByChild(uidSolicitante);
//
//        query.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot snapshot) {
//                tokenUsuarioQueSolicita = snapshot.getValue(String.class);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError error) {
//                // TODO Auto-generated method stub
//            }
//        });
//
//        database.getReference("notificaciones_status_reservacion").child(tokenUsuarioQueSolicita).setValue(notificacion,
//                new CompletionListener() {
//                    @Override
//                    public void onComplete(DatabaseError error, DatabaseReference ref) {
//                        System.out.println("Se realizó notificación de actualizacion de estatus reservación");
//                    }
//                });

    }

    public ReservationWithEvent getById(Long idReservation) {

        Reservation reservation = this.reservationRepo.findById(idReservation).orElse(null);
        String zonaHoraria = reservation.getRequestedPlace().getTimeZone();
        OffsetDateTime diaEventoConTiempoLocal = ConverterDate.toZoneIdFromUTC(reservation.getEventDay(), zonaHoraria);
        OffsetDateTime diaReservacionTiempoLocal = ConverterDate.toZoneIdFromUTC(reservation.getReservationDay(), zonaHoraria);
        reservation.setEventDay(diaEventoConTiempoLocal);
        reservation.setReservationDay(diaReservacionTiempoLocal);
        Events myEvent = this.eventRepo.findByIdReservation(idReservation);

        ReservationWithEvent reservConEvento = new ReservationWithEvent(ReservationWithEvent.ES_MI_RESERVACION,
                reservation, myEvent);
        return reservConEvento;
    }

    public ReservationWithHostWrapper getReservationWithHost(Long idReservation) {
        Reservation reservation = this.reservationRepo.findById(idReservation).orElse(null);
        String zonaHoraria = reservation.getRequestedPlace().getTimeZone();
        OffsetDateTime diaEventoConTiempoLocal = ConverterDate.toZoneIdFromUTC(reservation.getEventDay(), zonaHoraria);
        reservation.setEventDay(diaEventoConTiempoLocal);
        Events event = this.eventRepo.findByIdReservation(reservation.getId());

        ReservationWithHostWrapper reservationWithHostWrapper = new ReservationWithHostWrapper();
        reservationWithHostWrapper.setEvent(event);
        reservationWithHostWrapper.setHost(reservation.findApplicant());
        return reservationWithHostWrapper;
    }

    public List<OffsetDateTime> getAgenda(Long idUser) {
        User user = userRepo.findById(idUser).orElse(null);

        OffsetDateTime now = OffsetDateTime.now();
        // now= now.plusMonths(3);
        OffsetDateTime mesAnterior = now.minusMonths(1);
        String inicioBusqueda = mesAnterior.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        System.out.println("NOW: " + inicioBusqueda);

        // reservationRepo.findAll();

        List<Reservation> misReservaciones = reservationRepo.findReservationFromNow(user.getId(), inicioBusqueda);// user.getReservations();
        List<AdditionalServiceReservation> misReservacionesExtra = additionalServiceReservationRepo
                .findReservationServAdditionalFromNow(user.getId(), inicioBusqueda);
        // System.out.println("NumReservaciones: " + misReservaciones.size());
        List<Guest> misInvitacionesAEventos = user.AllGuests();// reservationRepo.findReservationEventsFromNow(user.getId(),
        // inicioBusqueda);
//		System.out.println("NumReservaciones: " + misInvitacionesAEventos.size());
        List<OffsetDateTime> miAgenda = new ArrayList<>();

        for (Reservation reservation : misReservaciones) {

            miAgenda.add(ConverterDate.toZoneIdFromUTC(reservation.getEventDay(),
                    reservation.getRequestedPlace().getTimeZone()));
        }

        for (Guest invitacion : misInvitacionesAEventos) {
            Reservation reservacionDelEvento = invitacion.getEvent().getReservation();
            miAgenda.add(reservacionDelEvento.getEventDay());

        }

        for (AdditionalServiceReservation reservationServAdditional : misReservacionesExtra) {
            System.out.println("Reservacion de extra en " + reservationServAdditional.getReservationDayEvent());
            miAgenda.add(reservationServAdditional.getReservationDayEvent());
        }

//        miAgenda.add(now);
        return miAgenda;
    }

    public List<ReservationWithEvent> getAgendaPorDia(Long idUser, int year, int month, int day) {
        User user = userRepo.findById(idUser).orElse(null);
        OffsetDateTime fecha = OffsetDateTime.now();// OffsetDateTime.of(year, month, day, 0, 0, 0, 0, ZoneOffset.UTC);
        String formatoFecha = fecha.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        System.out.println("Fecha: " + OffsetDateTime.now());
        List<Reservation> misReservaciones = this.reservationRepo.findReservationFromNow(idUser, formatoFecha);// findReservationsFromDate(idUser,
        // formatoFecha);
        List<Reservation> misInvitaciones = this.reservationRepo.findInvitationFromNow(idUser, formatoFecha);// findInvitacionesFromDate(idUser,
        List<AdditionalServiceReservation> misReservacionesExtra = this.additionalServiceReservationRepo
                .findReservationServAdditionalFromNow(idUser, formatoFecha);
        // formatoFecha);
        List<ReservationWithEvent> misRecintosOExtrasDelDia = new ArrayList<>();

        for (Reservation reservation : misReservaciones) {
//			System.out.println("dia que hizo reservacion: " + reservation.getReservationDay());
//			System.out.println("dia que quiere el evento: " + reservation.getEventDay());
            String zonaHoraria = reservation.getRequestedPlace().getTimeZone();
            OffsetDateTime diaConTiempoLocal = ConverterDate.toZoneIdFromUTC(reservation.getEventDay(), zonaHoraria);
            ValueParty valorPagadoPorUsuario = valuePartyRepo.findByTableAndKey("reservaciones", "pago_usuario");
            if (diaConTiempoLocal.getYear() == year && diaConTiempoLocal.getMonthValue() == month
                    && diaConTiempoLocal.getDayOfMonth() == day) {
                reservation.setEventDay(diaConTiempoLocal);
                if (reservation.getStatus().equals(valorPagadoPorUsuario.getValue())) {
                    Events myEvent = eventRepo.findByIdReservation(reservation.getId());
                    System.out.println("nombre mi evento: " + myEvent.getName());
                    misRecintosOExtrasDelDia.add(
                            new ReservationWithEvent(ReservationWithEvent.ES_MI_RESERVACION, reservation, myEvent));
                } else {
                    System.out.println("Sin evento aun la reserv " + reservation.getId() + " con status "
                            + reservation.getStatus());
                    misRecintosOExtrasDelDia
                            .add(new ReservationWithEvent(ReservationWithEvent.ES_MI_RESERVACION, reservation));
                }
            }
        }

        for (Reservation reservation : misInvitaciones) {
            String zonaHoraria = reservation.getRequestedPlace().getTimeZone();
            OffsetDateTime diaConTiempoLocal = ConverterDate.toZoneIdFromUTC(reservation.getEventDay(), zonaHoraria);
            if (diaConTiempoLocal.getYear() == year && diaConTiempoLocal.getMonthValue() == month
                    && diaConTiempoLocal.getDayOfMonth() == day) {
                reservation.setEventDay(diaConTiempoLocal);
                Events eventoInvitacion = eventRepo.findByIdReservation(reservation.getId());
                System.out.println("nombre evento: " + eventoInvitacion.getName());
                misRecintosOExtrasDelDia.add(
                        new ReservationWithEvent(ReservationWithEvent.SOY_INVITADO, reservation, eventoInvitacion));
            }
        }

        for (AdditionalServiceReservation reservacionExtra : misReservacionesExtra) {
            OffsetDateTime fechaReservacion = reservacionExtra.getReservationDayEvent();

            if (fechaReservacion.getYear() == year && fechaReservacion.getMonthValue() == month
                    && fechaReservacion.getDayOfMonth() == day) {
                System.out.println("Se tiene un extra de: " + reservacionExtra.getAdditionalService().getName());
                misRecintosOExtrasDelDia
                        .add(new ReservationWithEvent(ReservationWithEvent.ES_RESERVACION_EXTRA, reservacionExtra));
            }
        }
        return misRecintosOExtrasDelDia;
    }

    public void updateReservationFirebase(Long idReservacion, String status) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference hopperRef = database.getReference("Reservation").child(idReservacion.toString());
        Map<String, Object> hopperUpdates = new HashMap<>();
        hopperUpdates.put("status", status);
        Reservation reservation = this.reservationRepo.findById(idReservacion).orElse(null);
        hopperRef.updateChildrenAsync(hopperUpdates);

        System.out.println("la reservacion es " + idReservacion + " con estatus " + status);

        // Problemas por que me regresa el mismo valor a pesar de hacer las consultas
        // con diferentes parametros
//		ValueParty valorConfirmado = this.valuePartyRepo.findByTableAndKey("reservaciones", "confirmado");
//		ValueParty valorChatAbierto = this.valuePartyRepo.findByTableAndKey("reservaciones", "sala_chat");
//		ValueParty valorGeneraTicketProp = this.valuePartyRepo.findByTableAndKey("reservaciones",
//				"genera_pago_propietario");
//		ValueParty valorCanceladoRev = this.valuePartyRepo.findByTableAndKey("reservaciones", "cancelada");
//		ValueParty valorUsuarioPago = this.valuePartyRepo.findByTableAndKey("reservaciones", "pago_usuario");

        String chatAbierto = "6";// valorChatAbierto.getValue();
        String confirmado = "2";// valorConfirmado.getValue();
        String ticketGenerado = "3";// valorGeneraTicketProp.getValue();
        String cancelado = "4";// valorCanceladoRev.getValue();
        String pagoUsuario = "5";// valorUsuarioPago.getValue();

        // System.out.println("status from table: " + chatAbierto+
        // "-"+confirmado+"-"+ticketGenerado+"-"+cancelado+"-"+pagoUsuario);
        String leyendaEstatus = "";

        if (status.equals(chatAbierto)) {
            leyendaEstatus = "Ya puedes platicar con el propietario";
        }
        if (status.equals(confirmado)) {
            leyendaEstatus = "El propietario confirmó el evento. Recuerda que falta generar el ticket de pago";
        }
        if (status.equals(ticketGenerado)) {
            leyendaEstatus = "Genial!! El propietario ha generado el ticket, procede a realizar el pago";
        }
        if (status.equals(cancelado)) {
            leyendaEstatus = "Lo sentimos: el propietario ha cancelado tu reservación";
        }
        if (status.equals(pagoUsuario)) {
            leyendaEstatus = "Felicidades!!! estas listo para tu party";
        }

        ReservacionNotificacion notificacion = new ReservacionNotificacion(idReservacion);
        notificacion.setTitle(reservation.getRequestedPlace().getName());
        notificacion.setBody(leyendaEstatus);
        notificacion.setPhotoApplicant("");
        notificacion.setTipoNotificacion(Notificacion.ESTATUS_ACTUALIZADO_DE_RESERVACION);
        notificacion.setIdApplicant(reservation.findApplicant().getId());

        // System.out.println("leyenda status: " + leyendaEstatus);
        // System.out.println("Body de la reservacion: " + notificacion.getBody());

        database.getReference("notificaciones_status_reservacion").child(idReservacion.toString())
                .setValue(notificacion, new CompletionListener() {

                    @Override
                    public void onComplete(DatabaseError error, DatabaseReference ref) {
                        System.out.println("Se realizó notificación de actualizacion de estatus reservación");
                    }
                });

    }

    public List<Reservation> getbyTicket() {

        List<Reservation> reservation = this.reservationRepo.findReservationStatusTicketGenereted("3");

        return reservation;
    }

    private long setTypeReservation(OffsetDateTime reservationDay) {
        OffsetDateTime now = OffsetDateTime.now(Clock.systemUTC());
        Duration duration = Duration.between(now.toLocalDateTime(), reservationDay.toLocalDateTime());
        System.out.println(now);
        long totalDays = duration.toDays();
        long typeReservation = 0;
        if (totalDays > 45) {
            return 1;
        } else {
            if (totalDays < 45 && totalDays >= 31) {
                return 2;
            }
        }

        if (totalDays <= 30 && totalDays >= 15) {
            return 3;
        } else {
            if (totalDays <= 14) {
                return 4;
            }
        }
        return typeReservation;

    }
}
