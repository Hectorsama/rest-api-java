package com.partyOn.partyOn_backend.service;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.DatabaseReference.CompletionListener;
import com.partyOn.partyOn_backend.model.AdditionalService;
import com.partyOn.partyOn_backend.model.Category;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.model.firebase.AdditionalServiceNotificacion;
import com.partyOn.partyOn_backend.model.firebase.Notificacion;
import com.partyOn.partyOn_backend.repo.AdditionalServiceRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdditionalServiceService {
	@Autowired
	private AdditionalServiceRepo additionalServiceRepo;

	private String tokenApplicant = "";

	public Iterable<AdditionalService> getAll() {
		return this.additionalServiceRepo.findAll();
	}

	public void insert(AdditionalService additionalService) {
		this.additionalServiceRepo.save(additionalService);
	}

	public void notificacionSolicitudExtra(AdditionalService nwServicio, User user) {
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		AdditionalServiceNotificacion notificacion = new AdditionalServiceNotificacion(nwServicio.getId());
		notificacion.setTipoNotificacion(Notificacion.NUEVO_EXTRA_SOLICITADO);
		notificacion.setTitle(user.getName());
		notificacion.setBody(nwServicio.getName());
		notificacion.setImgExtra(nwServicio.getImgSrc());

		database.getReference("notificaciones_solicitud_extra").child(user.getUidFirebase()).setValue(notificacion,
				new CompletionListener() {

					@Override
					public void onComplete(DatabaseError error, DatabaseReference ref) {
						System.out.println("Se realizó notificación de solicitud de extra");
					}
				});
	}

}
