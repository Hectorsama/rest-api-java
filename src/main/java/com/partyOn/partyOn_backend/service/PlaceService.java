package com.partyOn.partyOn_backend.service;

import com.google.maps.GeoApiContext;
import com.google.maps.PendingResult;
import com.google.maps.TimeZoneApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import com.partyOn.partyOn_backend.jsonAdapter.PlaceDetailsFavWrapper;
import com.partyOn.partyOn_backend.jsonAdapter.PlaceRequestWrapper;
import com.partyOn.partyOn_backend.mail.MailService;
import com.partyOn.partyOn_backend.mail.Mails;
import com.partyOn.partyOn_backend.model.*;
import com.partyOn.partyOn_backend.repo.*;
import com.partyOn.partyOn_backend.util.ConverterDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.*;
import java.util.*;

@Service

public class PlaceService {
    @Autowired
    private PlacesRepo placesRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private CategoryRepo categoryRepo;
    @Autowired
    private PropertyTypeRepo propertyTypeRepo;

    @Autowired
    private PackagePartyRepo packagePartyRepo;
    @Autowired
    private BusyDayRepository busyRepo;

    @Autowired
    private ValuePartyRepo valuePartyRepo;

    @Autowired
    private ServiceRepo serviceRepo;

    @Autowired
    private PhotosRepo photoRepo;
    @Autowired
    private PaymentPlaceRepo paymentPlaceRepo;

    @Autowired
    private PaymentPlaceService paymentService;

    @Autowired
    private PlaceService placeService;

    @Autowired
    MailService mailService;

    @Autowired
    private Environment env;


    @Transactional
    public void insert(PlaceRequestWrapper placeRequestWrapper, Long idUser) throws Exception {
        Long idPaquete = null;
        System.out.println(placeRequestWrapper.toString());
        Place place = placeRequestWrapper.getPlace();
        User user = this.userRepo.findById(idUser).orElse(null);
        String zonaHorariaDelLugar = "";
        PropertyType propertyType = this.propertyTypeRepo.findBySubId(placeRequestWrapper.getPropertyType().getSubId());
        PackageParty packageParty = this.packagePartyRepo.findByName(placeRequestWrapper.getPackageParty().getName());
        ValueParty value = this.valuePartyRepo.findByTableAndKey("lugares","activo");//("lugares", "revision");
        GeoApiContext context = new GeoApiContext.Builder().apiKey("AIzaSyCKnefAG8SHrDzTQdTgl0aC0kinjiJXyq4").build();
        try {
            TimeZone zonaHoraria = TimeZoneApi
                    .getTimeZone(context, new LatLng(place.getLatitude(), place.getLongitude())).await();
            zonaHorariaDelLugar = zonaHoraria.getID();
            place.setTimeZone(zonaHorariaDelLugar);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        for (Category item : placeRequestWrapper.getCategory()) {
            Category category = this.categoryRepo.findByShortName(item.getSubId());
            place.insert_category(category);
        }

        for (Services item : placeRequestWrapper.getServices()) {
            System.out.println(item.getSubId());
            Services services = this.serviceRepo.findBySubId(item.getSubId());
            place.insert_service(services);
        }

        for (BusyDay item : placeRequestWrapper.getBusyDays()) {
            System.out.println("Recibo el dia: " + item.getDate());
            OffsetDateTime dateInUTC = ConverterDate.toZoneIdInUTC(item.getDate(), zonaHorariaDelLugar);
            System.out.println("Lo convierto en el dia: " + dateInUTC);
            BusyDay busyDay = new BusyDay();
            busyDay.setPlace(place);
            busyDay.setDate(dateInUTC);
            //item.setDate(dateInUTC);
            //item.setPlace(place);
            place.insertBusyDay(busyDay);
        }

        for (Photos item : placeRequestWrapper.getPhotos()) {
            item.setPlace(place);
            place.insert_photos(item);
        }
        place.setUser(user);
        place.setPropertyType(propertyType);
        place.setPackageParty(packageParty);
        place.setStatus(value.getValue());
        place.setRating(5.0);
        Long id = this.placesRepo.save(place).getId();
        System.out.println("------------->" + id.toString());
        String mail_env = env.getProperty("MAIL_PARTYON");
        Place placeEmail = this.placeService.getById(id);

        if (packageParty.getName().equals("PartyOnPlatinum")) {
            this.paymentService.add(id);
            /**Mail to applicant*/
            Mails mail = new Mails();
            mail.setMailFrom(mail_env);
            mail.setMailTo(user.getEmail());
            mail.setMailSubject("Solicitud de registro de un recinto con paquete");


            Map<String, Object> model = new HashMap<String, Object>();
            model.put("name_place", placeEmail.getName());
            model.put("price", placeEmail.findPackageParty().getAnnualSubscription());

            mail.setModel(model);
            mailService.sendEmailPlacePackage(mail);

        } else {
            /**Mail to applicant*/
            Mails mail = new Mails();
            mail.setMailFrom(mail_env);
            mail.setMailTo(user.getEmail());
            mail.setMailSubject("Solicitud de registro de un recinto");

            Map<String, Object> model = new HashMap<String, Object>();
            model.put("name_place", placeEmail.getName() );
            mail.setModel(model);
            mailService.sendEmailPlace(mail);

        }

    }

    public Iterable<Place> getAll() {
        return this.placesRepo.findAll();
    }

    public Place getById(Long id) {
        Place place = this.placesRepo.findById(id).orElse(null);
//		for (BusyDay busyDay : place.allBusyDays()) {
//			System.out.println(busyDay.getDate());
//		}
        return place;
    }

    public PlaceDetailsFavWrapper getByIdDetails(Long idPlace, User user, Boolean isLogin) {

        Place place = this.placesRepo.findById(idPlace).orElse(null);
        User owner = place.findUser();
        owner.setLikes(new ArrayList<>());
        owner.setReservations(new ArrayList<>());
        PlaceDetailsFavWrapper placeDetails = new PlaceDetailsFavWrapper();
        placeDetails.setPlace(place);
        placeDetails.setOwner(owner);

        if (isLogin) {
            int hayFav = this.userRepo.existeFavorito(idPlace, user.getId());
            if (hayFav == 1) {
                placeDetails.setIsFav(true);
            } else {
                placeDetails.setIsFav(false);
            }
        } else {
            placeDetails.setIsFav(false);
        }
        return placeDetails;
    }

    public List<Place> getByCategory(String name, Double latitud, Double longitud, int radioKm) {
//        Category category = this.categoryRepo.findByShortName(name);
        Iterable<Place> lugaresEncontrados = this.placesRepo.findByCategory(name, latitud, longitud, radioKm);
        List<Place> infoEsencial = new ArrayList<>();

        for (Place place : lugaresEncontrados) {
            Place temp = place;
            temp.setCategories(null);
            temp.setServices(null);
            infoEsencial.add(temp);
        }

        return infoEsencial;// lugaresEncontrados;
    }

    public List<Place> getByLocation(Double latitud, Double longitud, int radioKm) {
        Iterable<Place> lugaresEncontrados = this.placesRepo.findByLatLng(latitud, longitud, radioKm);
        List<Place> infoEsencial = new ArrayList<>();

        for (Place place : lugaresEncontrados) {
            Place temp = place;
            temp.setCategories(null);
            temp.setServices(null);
            infoEsencial.add(temp);
        }

        return infoEsencial;// lugaresEncontrados;
    }

//    @Transactional
//    public void getTimeZone() {
//
//        try {
//            Iterable<Place> todos = this.placesRepo.findAll();
//            GeoApiContext context = new GeoApiContext.Builder().apiKey("AIzaSyCKnefAG8SHrDzTQdTgl0aC0kinjiJXyq4")
//                    .build();
//
//            for (Place place : todos) {
//                TimeZone respuesta = TimeZoneApi
//                        .getTimeZone(context, new LatLng(place.getLatitude(), place.getLongitude())).await();
//
//                place.setTimeZone(respuesta.getID());
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
    
    
    public List<Place> recintosPorRevisar(){
    	ValueParty porRevisar= this.valuePartyRepo.findByTableAndKey("lugares", "revision");
    	return this.placesRepo.findByStatus(porRevisar.getValue());
    }
    
    @Transactional
    public void activaRecinto(Long idRecinto){
    	ValueParty activadoValue= this.valuePartyRepo.findByTableAndKey("lugares", "activo");
    	Place place= this.placesRepo.findById(idRecinto).orElse(null);
    	place.setStatus(activadoValue.getValue());
    }
}
