package com.partyOn.partyOn_backend.service;


import com.partyOn.partyOn_backend.mail.MailService;
import com.partyOn.partyOn_backend.mail.Mails;
import com.partyOn.partyOn_backend.model.AdditionalService;
import com.partyOn.partyOn_backend.model.AdditionalServiceReservation;
import com.partyOn.partyOn_backend.model.Place;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.repo.AdditionalServiceRepo;
import com.partyOn.partyOn_backend.repo.AdditionalServiceReservationRepo;
import com.partyOn.partyOn_backend.repo.UserRepo;
import com.partyOn.partyOn_backend.util.ConverterDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Clock;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdditionalServiceReservationService {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private AdditionalServiceReservationRepo additionalServiceReservationRepo;

    @Autowired
    private AdditionalServiceRepo additionalServiceRepo;

    @Autowired
    private Environment env;

    @Autowired
    private MailService mailService;

    @Autowired
    AdditionalServiceService service;

    @Transactional
    public void insert(AdditionalServiceReservation reservation, Long idUser, Long idService) throws Exception {
        User user = this.userRepo.findById(idUser).orElse(null);
        AdditionalService additionalService = this.additionalServiceRepo.findById(idService).orElse(null);
        reservation.setUser(user);
        reservation.setAdditionalService(additionalService);
        OffsetDateTime offsetdatetime
                = OffsetDateTime.now(
                Clock.systemUTC());
        reservation.setReservationDay(offsetdatetime);
        additionalServiceReservationRepo.save(reservation);


        String mail_env = env.getProperty("MAIL_PARTYON");
        String mailAdmin = env.getProperty("MAIL_PARTYON_ADMIN");
        String mailAdmin2 = env.getProperty("MAIL_PARTYON_ADMIN2");
        String mailProvider = additionalService.getProvider().getEmail();

        ArrayList<String> mails = new ArrayList<>();

        //mails.add(mailProvider);
        mails.add(mailAdmin);
        mails.add(mailAdmin2);
        String note = "";
        String cp = "";
        if (reservation.getNote() != null && !reservation.getNote().equals("")) {
            note = reservation.getNote();
        } else {
            note = "No contiene datos";
        }

        if (reservation.getCp() != null) {
            cp = reservation.getCp();
        } else {
            cp = "No contiene datos";
        }

        for (String email : mails) {
            Mails mail = new Mails();
            mail.setMailFrom(mail_env);
            mail.setMailTo(email);
            mail.setMailSubject("Reservación de un extra");

            Map<String, Object> model = new HashMap<String, Object>();
            model.put("user", user.getName());
            model.put("quantity", reservation.getQuantity());
            model.put("note", note);
            model.put("cp", cp);
            model.put("dateReservation", reservation.getReservationDayEvent().toLocalDate());
            model.put("product", additionalService.getName());
            model.put("price", additionalService.getPrice().toString());
            model.put("phone", user.getPhone());
            model.put("mail", user.getEmail());
            model.put("date", LocalDate.now().toString());
            model.put("totalPrice", reservation.getTotalCost());
            mail.setModel(model);

            mailService.sendEmail(mail);
        }


        Mails mail_user = new Mails();
        mail_user.setMailFrom(mail_env);
        mail_user.setMailTo(user.getEmail());
        mail_user.setMailSubject("Reservación de un extra");

        Map<String, Object> model_user = new HashMap<String, Object>();
        model_user.put("product", additionalService.getName());
        model_user.put("price", additionalService.getPrice().toString());
        model_user.put("priceTotal", reservation.getTotalCost());
        model_user.put("dateReservation", reservation.getReservationDayEvent().toLocalDate());
        model_user.put("date", LocalDate.now().toString());
        mail_user.setModel(model_user);

        mailService.sendEmailExtrasUser(mail_user);
        System.out.println("Done!");

        this.service.notificacionSolicitudExtra(additionalService, user);
    }

    @Transactional
    public Iterable<AdditionalServiceReservation> getAll() throws Exception {
        return this.additionalServiceReservationRepo.findAll();
    }
}
