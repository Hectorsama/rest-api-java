package com.partyOn.partyOn_backend.service;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.partyOn.partyOn_backend.model.Payment;
import com.partyOn.partyOn_backend.model.Reservation;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.repo.PaymentRepo;
import com.partyOn.partyOn_backend.repo.UserRepo;
import com.partyOn.partyOn_backend.util.ConverterDate;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepo paymentRepo;
    @Autowired
    private UserRepo userRepo;

    public void add(Payment payment, Long id) {
        User user = this.userRepo.findById(id).orElse(null);
        payment.setUser(user);
        this.paymentRepo.save(payment);
    }

    public Payment getById(Long id) {
        return this.paymentRepo.findById(id).orElse(null);
    }

    /**
     * Verificar que las fechas de la reservacion del pago estan en la fecha local
     * @param idUser
     * @return pagos con reservaciones que tienen las horas en formato local
     */
    public List<Payment> getPaymentFromUser(Long idUser) {
        User user = this.userRepo.findById(idUser).orElse(null);
        List <Payment>pagosConFechasNormalizadas = new ArrayList<>();
        List<Payment> pagosDeUsuario = user.AllPayments();
        
        for(Payment payment: pagosDeUsuario) {
        	Reservation reservation = payment.getReservation();
        	String zonaHoraria= reservation.getRequestedPlace().getTimeZone();
			OffsetDateTime diaEventoConTiempoLocal = ConverterDate.toZoneIdFromUTC(reservation.getEventDay(), zonaHoraria);
			OffsetDateTime diaReservacionTiempoLocal= ConverterDate.toZoneIdFromUTC(reservation.getReservationDay(), zonaHoraria);
			
			payment.getReservation().setEventDay(diaEventoConTiempoLocal);
			payment.getReservation().setReservationDay(diaReservacionTiempoLocal);
			
			pagosConFechasNormalizadas.add(payment);
        }
        return pagosConFechasNormalizadas;//user.AllPayments();
    }



}
