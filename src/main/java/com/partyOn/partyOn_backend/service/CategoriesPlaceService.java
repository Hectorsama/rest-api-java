package com.partyOn.partyOn_backend.service;

import com.partyOn.partyOn_backend.model.Category;
import com.partyOn.partyOn_backend.model.Place;
import com.partyOn.partyOn_backend.model.Services;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.repo.CategoryRepo;
import com.partyOn.partyOn_backend.repo.PlacesRepo;
import com.partyOn.partyOn_backend.repo.ServiceRepo;
import com.partyOn.partyOn_backend.repo.UserRepo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class CategoriesPlaceService {

    private static final Log log = LogFactory.getLog(FavoriteService.class);
    @Autowired
    private CategoryRepo categoryRepo;

    @Autowired
    private PlacesRepo placeRepo;

    @Autowired
    private ServiceRepo serviceRepo;

    @Transactional
    public void insertCategoriesPlaces(Long idPlace, Long idCategory) {
        Place place = this.placeRepo.findById(idPlace).orElse(null);
        Category category = this.categoryRepo.findById(idCategory).orElse(null);
        place.insert_category(category);

    }

    @Transactional
    public void insertServicePlaces(Long idPlace, Long idService) {
        Place place = this.placeRepo.findById(idPlace).orElse(null);
        Services services = this.serviceRepo.findById(idService).orElse(null);
        place.insert_service(services);

    }

    public Long findByNameIdC(Category category) {
        return categoryRepo.findByNameId(category.getCategory());
    }

//    public Long findByNameIdS(Services service) {
//        return serviceRepo.findByNameId(service.getName());
//    }
//
//
//    public Long findBySubIdS(Services service) {
//        return serviceRepo.findBySubId(service.getSub_id());
//    }

	
	  public Long findBySubIdC(Category category) { return
	  categoryRepo.findBySubId(category.getSubId()); }
	 

}