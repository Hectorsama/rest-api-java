package com.partyOn.partyOn_backend.service;

import com.partyOn.partyOn_backend.model.*;
import com.partyOn.partyOn_backend.repo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class GuestService {
    @Autowired
    private GuestRepo guestRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private EventsRepo eventRepo;

    @Transactional
    public void insert(Long idUser, Long idEvent, String status) {
        User user = userRepo.findById(idUser).orElse(null);
        Events event = eventRepo.findById(idEvent).orElse(null);

        Guest guest = new Guest();
        guest.setUser(user);
        guest.setEvent(event);
        guest.setStatus(status);
        guestRepo.save(guest);
    }
}
