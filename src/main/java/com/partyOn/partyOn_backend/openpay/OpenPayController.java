package com.partyOn.partyOn_backend.openpay;


import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.mail.MailService;
import com.partyOn.partyOn_backend.mail.Mails;
import com.partyOn.partyOn_backend.model.*;
import com.partyOn.partyOn_backend.repo.ReservationRepo;
import com.partyOn.partyOn_backend.service.PaymentService;
import com.partyOn.partyOn_backend.service.ReservationService;
import com.partyOn.partyOn_backend.service.UserService;

import mx.openpay.client.*;
import mx.openpay.client.core.OpenpayAPI;

import mx.openpay.client.core.requests.transactions.CreateCardChargeParams;

import mx.openpay.client.exceptions.OpenpayServiceException;
import mx.openpay.client.exceptions.ServiceUnavailableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping({"/openpay"})
public class OpenPayController {
    @Autowired
    FireBaseService fireBaseService;
    @Autowired
    private ReservationService reservationService;
    @Autowired
	private ReservationRepo reservationRepo;
    @Autowired
    private UserService userService;
    @Autowired
    private Environment env;
    @Autowired
    MailService mailService;
    @Autowired
    private PaymentService paymentService;

    /**
     * Para la versión de prueba de open pay la url https://sandbox-api.openpay.mx/v1/ no funciona correctamente
     */
    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public ResponseEntity<String> restCall(@RequestHeader(value = "Authorization") String firebaseToken, @RequestBody Token token, @RequestParam("reservation") Long idRevervation) throws Exception {
        System.out.println("AQUI-------->" + idRevervation.toString());
        String location = env.getProperty("LOCATION");
        String apiKey = env.getProperty("APIKEY");
        String merchantId = env.getProperty("MERCHANTID");
        TokenInfo tokenFirebase = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

        if (tokenFirebase.isValidated()) {
            try {


                Reservation reservation = this.reservationRepo.findById(idRevervation).orElse(null);
                User user = reservation.findApplicant();
                BigDecimal pricePlace = reservation.getTotalCost();
                BigDecimal chargeReservation = pricePlace.multiply(new BigDecimal(".05"));
                BigDecimal totalPrice = chargeReservation.add(pricePlace);
                OpenpayAPI api = new OpenpayAPI(location, apiKey, merchantId);
                String device = token.getDevice();
                String id = token.getToken().getId();
                String holderName = token.getToken().getCard().getHolderName();
                String orderId = "oid-" + idRevervation.toString();
                /**Split firstname and lastname*/
                String fullName = holderName;
                int idx = fullName.lastIndexOf(' ');
                if (idx == -1)
                    throw new IllegalArgumentException("Only a single name: " + fullName);
                String firstName = fullName.substring(0, idx);
                String lastName = fullName.substring(idx + 1);

                try {
                    Customer customer = storeCustomer(firstName, lastName, user.getPhone(), user.getEmail());
                    CreateCardChargeParams charge = new CreateCardChargeParams()
                            .cardId(id)
                            .currency("MXN")
                            .amount(totalPrice)
                            .description("Pago Reservación Recinto")
                            .orderId(orderId)
                            .deviceSessionId(device)
                            .customer(customer);

                    Charge charges = api.charges().create(charge);

                    Payment payment = new Payment();
                    payment.setOrderNumber(orderId);
                    payment.setReservation(reservation);
                    OffsetDateTime offsetdatetime
                            = OffsetDateTime.now(
                            Clock.systemUTC());
                    payment.setPayDay(offsetdatetime);
                    this.paymentService.add(payment, user.getId());

                } catch (OpenpayServiceException e) {
                    e.printStackTrace();
                    return new ResponseEntity<String>("Error", HttpStatus.valueOf(e.getHttpCode()));
                }
                this.reservationService.payReservation(idRevervation, user);

                try {
                    final String mail_env = env.getProperty("MAIL_PARTYON");
                    Mails mail = new Mails();
                    mail.setMailFrom(mail_env);
                    mail.setMailTo(user.getEmail());
                    mail.setMailSubject("Reservación de un recinto");

                    Map<String, Object> model = new HashMap<String, Object>();
                    model.put("dia", reservation.getEventDay().getDayOfMonth());
                    model.put("mes", reservation.getEventDay().getMonthValue());
                    model.put("year", reservation.getEventDay().getYear());
                    model.put("date", reservation.getEventDay());
                    model.put("name", reservation.getRequestedPlace().findUser().getName());
                    model.put("name_place", reservation.getRequestedPlace().getName());
                    model.put("price", reservation.getTotalCost().toString());
                    model.put("mail", reservation.getRequestedPlace().findUser().getEmail());
                    mail.setModel(model);
                    mailService.sendEmailPlaceUser(mail);


                    final String _mail_env = env.getProperty("MAIL_PARTYON");
                    Mails _mail = new Mails();
                    _mail.setMailFrom(_mail_env);
                    _mail.setMailTo(reservation.getRequestedPlace().findUser().getEmail());
                    _mail.setMailSubject("Reservación de un recinto");

                    Map<String, Object> _model = new HashMap<String, Object>();
                    _model.put("dia", reservation.getEventDay().getDayOfMonth());
                    _model.put("mes", reservation.getEventDay().getMonth());
                    _model.put("year", reservation.getEventDay().getYear());
                    _model.put("nombre_cliente", user.getName());
                    _model.put("nombre", reservation.getRequestedPlace().getName());
                    _model.put("price", reservation.getTotalCost().toString());
                    _model.put("correo", user.getEmail());
                    _mail.setModel(_model);
                    mailService.sendEmailPlaceOwner(_mail);
                } catch (Exception e) {
                    e.printStackTrace();
                    //  return new ResponseEntity<String>("Error mail service", HttpStatus.BAD_REQUEST);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<String>("User not found", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<String>("Succesfully", HttpStatus.OK);
        }

        return new ResponseEntity<String>("Token Error", HttpStatus.UNAUTHORIZED);

    }


    public Customer storeCustomer(String name, String lastName, String phone, String email) throws OpenpayServiceException, ServiceUnavailableException {
        String location = env.getProperty("LOCATION");
        String apiKey = env.getProperty("APIKEY");
        String merchantId = env.getProperty("MERCHANTID");
        OpenpayAPI api = new OpenpayAPI(location, apiKey, merchantId);

        Customer customer = new Customer();
        customer.setName(name);
        customer.setLastName(lastName);
        customer.setPhoneNumber(phone);
        customer.setEmail(email);
        customer = api.customers().create(customer);
        System.out.println("TOKEN ID CUSTOMER" + customer.getId());
        return customer;
    }


    @RequestMapping(value = "/subcription", method = RequestMethod.POST)
    public ResponseEntity<String> subcription(@RequestHeader(value = "Authorization") String firebaseToken, @RequestBody Token token) throws OpenpayServiceException, ServiceUnavailableException {

        String location = env.getProperty("LOCATION");
        String apiKey = env.getProperty("APIKEY");
        String merchantId = env.getProperty("MERCHANTID");
        //TokenInfo tokenFirebase = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

        // if (tokenFirebase.isValidated()) {
        try {

            //final Calendar trialEndDate = Calendar.getInstance();
            //trialEndDate.set(2021, 5, 1, 0, 0, 0);
            OpenpayAPI api = new OpenpayAPI(location, apiKey, merchantId);

            Customer customer = new Customer();
            customer.setName("Prueba");
            customer.setLastName(token.getToken().getCard().getHolderName());
            customer.setPhoneNumber("430234312");
            customer.setEmail("prueba@gmail.com");
            customer = api.customers().create(customer);
            String id = token.getToken().getId();
            Subscription request = new Subscription();
            /**Valor determinado por OPENPAY*/
            request.planId("po8wc4gmkn4ybqygx2am");
            request.setCardId(id);

            request = api.subscriptions().create(customer.getId(), request);
            return new ResponseEntity<String>("Succesfully", HttpStatus.OK);

        } catch (OpenpayServiceException e) {

            return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
        }
        //}

        // return new ResponseEntity<String>("Token Error", HttpStatus.UNAUTHORIZED);

    }

}
