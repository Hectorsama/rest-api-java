package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.model.Category;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({"/category"})
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResponseEntity<?> insert_category(@RequestBody Category category) {
        try {
            this.categoryService.insert(category);
            return new ResponseEntity<String>("Store category succesfully", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        try {
            return new ResponseEntity<Iterable<Category>>(this.categoryService.getAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "getById/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<Category>(this.categoryService.getById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
        }
    }
}
