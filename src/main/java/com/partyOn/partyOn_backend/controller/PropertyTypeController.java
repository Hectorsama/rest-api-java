package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.model.Category;
import com.partyOn.partyOn_backend.model.PropertyType;
import com.partyOn.partyOn_backend.service.CategoryService;
import com.partyOn.partyOn_backend.service.PropertyTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping({"/property"})
public class PropertyTypeController {
    @Autowired
    PropertyTypeService propertyTypeService;

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResponseEntity<?> insert_category(@RequestBody PropertyType propertyType) {
        try {
            this.propertyTypeService.insert(propertyType);
            return new ResponseEntity<String>("Store propertyType succesfully", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
        }
    }
}
