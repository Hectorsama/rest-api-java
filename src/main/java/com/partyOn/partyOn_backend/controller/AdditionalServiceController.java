package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.jsonAdapter.PlaceRequestWrapper;
import com.partyOn.partyOn_backend.mail.MailService;
import com.partyOn.partyOn_backend.mail.Mails;
import com.partyOn.partyOn_backend.model.*;
import com.partyOn.partyOn_backend.repo.AdditionalServiceRepo;
import com.partyOn.partyOn_backend.repo.UserRepo;
import com.partyOn.partyOn_backend.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping({"/additionalService"})
public class AdditionalServiceController {
    @Autowired
    AdditionalServiceService service;

    @Autowired
    AdditionalServiceCategoryService additionalServiceCategoryService;
   
    @Autowired
    private FireBaseService fireBaseService;
    @Autowired
    private Environment env;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private AdditionalServiceRepo additionalServiceRepo;
    @Autowired
    MailService mailService;

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        try {
            return new ResponseEntity<Iterable<AdditionalService>>(this.service.getAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/getAllExtrasCategories", method = RequestMethod.GET)
    public ResponseEntity<?> getAllExtras() {
        try {
            return new ResponseEntity<Iterable<AdditionalServiceCategory>>(this.additionalServiceCategoryService.getAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
        }
    }

    /*@RequestMapping(value = "/getAllExtrasPackage", method = RequestMethod.GET)
    public ResponseEntity<?> getAllExtrasPackage() {
        try {
            return new ResponseEntity<Iterable<AdditionalServicePackage>>(this.additionalServicePackageService.getAll(),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
        }
    }*/

//    @RequestMapping(value = "/insert", method = RequestMethod.POST)
//    public ResponseEntity<?> insert(@RequestBody AdditionalService additionalService,
//                                    @RequestParam("subId") String subId) {
//        this.service.insert(additionalService);
//        try {
//            this.service.insert(additionalService);
//            Extra extra = new Extra();
//            AdditionalServiceCategory additionalServiceCategory = extraCategoryService.findSubId(subId);
//            extra.setExtraCategory(additionalServiceCategory);
//            extra.setAdditionalService(additionalService);
//            this.extrasService.insert(extra);
//            return new ResponseEntity<String>("Store Additional Service", HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
//        }
//
//    }

    @PostMapping(value = "/request-extra")
    public ResponseEntity<?> uptateReservation(@RequestHeader(value = "Authorization") String firebaseToken,
                                               @RequestParam(value = "servicioAdicional") Long idServicioAdicional) {
        TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));


        if (token.isValidated()) {
            try {
                AdditionalService nwServicio = additionalServiceRepo.findById(idServicioAdicional).orElse(null);
                User user = userRepo.findByUid(token.getUid());

                String mail_env = env.getProperty("MAIL_PARTYON");
                String mailAdmin = env.getProperty("MAIL_PARTYON_ADMIN");
                String mailAdmin2 = env.getProperty("MAIL_PARTYON_ADMIN2");
                String mailProvider = nwServicio.getProvider().getEmail();

                ArrayList<String> mails = new ArrayList<>();

                mails.add(mailProvider);
                mails.add(mailAdmin);
                mails.add(mailAdmin2);

                for (String email:mails) {
                    Mails mail = new Mails();
                    mail.setMailFrom(mail_env);
                    mail.setMailTo(email);
                    mail.setMailSubject("Reservación de un extra");

                    Map<String, Object> model = new HashMap<String, Object>();
                    model.put("user", user.getName());
                    model.put("product", nwServicio.getName());
                    model.put("price", nwServicio.getPrice().toString());
                    model.put("phone", user.getPhone());
                    model.put("mail", user.getEmail());
                    model.put("date", LocalDate.now().toString());
                    mail.setModel(model);

                    mailService.sendEmail(mail);
                }


                Mails mail_user = new Mails();
                mail_user.setMailFrom(mail_env);
                mail_user.setMailTo(user.getEmail());
                mail_user.setMailSubject("Reservación de un extra");

                Map<String, Object> model_user = new HashMap<String, Object>();
                model_user.put("product", nwServicio.getName());
                model_user.put("price", nwServicio.getPrice().toString());
                model_user.put("date", LocalDate.now().toString());
                mail_user.setModel(model_user);

                mailService.sendEmailExtrasUser(mail_user);
                System.out.println("Done!");

                this.service.notificacionSolicitudExtra(nwServicio, user);
                return new ResponseEntity<String>("Store ", HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
    }

}
