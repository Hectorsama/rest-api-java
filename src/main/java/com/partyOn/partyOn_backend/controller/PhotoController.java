package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.model.Payment;
import com.partyOn.partyOn_backend.model.Photos;
import com.partyOn.partyOn_backend.service.PaymentService;
import com.partyOn.partyOn_backend.service.PhotosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({"/photos"})
public class PhotoController {
    @Autowired
    private PhotosService photosService;

    @PostMapping(value = "/insert")
    public void insert_photo(@RequestBody Photos photos, @RequestParam("place_id") Long id) {
        this.photosService.insert(photos, id);
    }
}
