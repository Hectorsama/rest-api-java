package com.partyOn.partyOn_backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.partyOn.partyOn_backend.model.Provider;
import com.partyOn.partyOn_backend.service.ProviderService;

@RestController
@RequestMapping({ "/provider" })
public class ProviderController {

	@Autowired
	private ProviderService providerService;

	@GetMapping(value = "all")
	public Iterable<Provider> getAll() {
		return this.providerService.all();
	}
}
