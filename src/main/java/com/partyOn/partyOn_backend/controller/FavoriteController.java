package com.partyOn.partyOn_backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.model.Place;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.repo.UserRepo;
import com.partyOn.partyOn_backend.service.FavoriteService;
import com.partyOn.partyOn_backend.service.UserService;

@RestController
@RequestMapping(value = "/favorites")
public class FavoriteController {

	@Autowired
	private FavoriteService favoriteService;

	@Autowired
	FireBaseService fireBaseService;

	@Autowired
	private UserService userService;
	@Autowired
	private UserRepo userRepo;

//	@PostMapping(value = "/add")
//	public void addFavorito(@RequestParam("place") Long idPlace, @RequestParam("user") Long idUser) {
//		this.favoriteService.addFavorite(idPlace, idUser);
//	}

	@GetMapping(value = "/from-user")
	public ResponseEntity<?> favoritosDeUsuario(@RequestHeader(value = "Authorization") String firebaseToken) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		if (token.isValidated()) {
			try {
				User user = userService.findByUidFirebase(token.getUid());
				Long idUser = user.getId();
				return new ResponseEntity<List<Place>>(this.favoriteService.favoritos(idUser), HttpStatus.OK);

			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Token invalido", HttpStatus.UNAUTHORIZED);
	}

	@GetMapping(value = "/from-user-sin-token")
	public List<Place> favoritosDeUsuarioSinToken(@RequestParam("user") Long id) {
		return this.favoriteService.favoritos(id);
	}

	@PostMapping(value = "/toggle")
	public ResponseEntity<?> toggle(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("place") Long idPlace) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

		if (token.isValidated()) {
			try {

				User user = userService.findByUidFirebase(token.getUid());
				int favorito = this.userRepo.existeFavorito(idPlace, user.getId());

				if (favorito == 1) {
					System.out.println("Existe, procede a eliminar");
					this.favoriteService.dislike(idPlace, user.getId());
				} else if (favorito == 0) {
					System.out.println("No existe, procede a agregar");
					this.favoriteService.addFavorite(idPlace, user.getId());
				}
				return new ResponseEntity<String>("Ok", HttpStatus.OK);

			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}

		}
		return new ResponseEntity<String>("Token invalido", HttpStatus.UNAUTHORIZED);

	}

	@PostMapping(value = "/toggle-sin-token")
	public void toggleSinToken(@RequestParam("place") Long idPlace, @RequestParam("user") Long idUser) {
		User user = userService.getById(idUser);
		int favorito = this.userRepo.existeFavorito(idPlace, user.getId());
		if (favorito == 1) {
			this.favoriteService.dislike(idPlace, idUser);
		} else if (favorito == 0) {
			this.favoriteService.addFavorite(idPlace, idUser);
		}
	}

//	@DeleteMapping(value = "/dislike")
//	public void eliminaLike(@RequestParam("place") Long idPlace, @RequestParam("user") Long idUser) {
//		this.favoriteService.dislike(idPlace, idUser);
//	}
}
