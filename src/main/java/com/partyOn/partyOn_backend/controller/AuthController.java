package com.partyOn.partyOn_backend.controller;

import ch.qos.logback.core.db.dialect.SybaseSqlAnywhereDialect;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.repo.UserRepo;

import com.partyOn.partyOn_backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({ "/auth" })
public class AuthController {
	@Autowired
	FireBaseService fireBaseService;

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> restCall(@RequestHeader(value = "Authorization") String firebaseToken) {

		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		if (token.isValidated()) {
			User user = this.userService.findByUidFirebase(token.getUid());
			System.out.println(user.getUidFirebase());
			System.out.println(user.getEmail());
			return new ResponseEntity<Integer>(Integer.parseInt(user.getStatus()), HttpStatus.OK);
		}
		return new ResponseEntity<String>("Error", HttpStatus.FORBIDDEN);

	}

}
