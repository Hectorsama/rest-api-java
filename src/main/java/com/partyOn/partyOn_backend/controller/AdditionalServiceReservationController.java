package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.jsonAdapter.PlaceRequestWrapper;
import com.partyOn.partyOn_backend.model.AdditionalServiceReservation;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.service.AdditionalServiceReservationService;
import com.partyOn.partyOn_backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({"/additionalServiceReservation"})
public class AdditionalServiceReservationController {
    @Autowired
    FireBaseService fireBaseService;
    @Autowired
    private UserService userService;
    @Autowired
    private AdditionalServiceReservationService additionalServiceReservationService;

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResponseEntity<?> insert(@RequestHeader(value = "Authorization") String firebaseToken,
                                           @RequestParam("idService") Long idAdditionalService,@RequestBody AdditionalServiceReservation additionalServiceReservation) {

        TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
        if (token.isValidated()) {
            try {
                User user = userService.findByUidFirebase(token.getUid());
                Long idUser = user.getId();
                this.additionalServiceReservationService.insert(additionalServiceReservation ,idUser,idAdditionalService);
                return new ResponseEntity<String>("Store reservation successfully", HttpStatus.OK);

            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
            }
        }
        return new ResponseEntity<String>("Token Error", HttpStatus.UNAUTHORIZED);
    }

}
