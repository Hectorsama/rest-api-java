package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.model.PaymentPlace;
import com.partyOn.partyOn_backend.model.Reservation;
import com.partyOn.partyOn_backend.service.PaymentPlaceService;
import com.partyOn.partyOn_backend.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({ "/paymentPlace" })
public class PaymentPlaceController {
	@Autowired
	private PaymentPlaceService paymentService;

	@PostMapping(value = "/insert")
	public void newReservationSinToken(@RequestParam("id") Long idPlace) {
		this.paymentService.add(idPlace);
	}

	
}
