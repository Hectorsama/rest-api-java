package com.partyOn.partyOn_backend.controller;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.List;

import com.partyOn.partyOn_backend.model.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.jsonAdapter.ReservacionConsultaWrapper;
import com.partyOn.partyOn_backend.jsonAdapter.ReservationWithEvent;
import com.partyOn.partyOn_backend.jsonAdapter.ReservationWithHostWrapper;
import com.partyOn.partyOn_backend.model.Reservation;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.service.ReservationService;
import com.partyOn.partyOn_backend.service.UserService;

@RestController
@RequestMapping({ "/reservation" })
public class ReservationController {

	@Autowired
	private ReservationService reservationService;

	@Autowired
	FireBaseService fireBaseService;

	@Autowired
	private UserService userService;

	@Autowired
	private Environment env;

	@PostMapping(value = "/insert-sin-token")
	public Long newReservationSinToken(@RequestBody Reservation reservation, @RequestParam("user") Long idUser,
			@RequestParam("place") Long idPlace) {
		return this.reservationService.nuevaReservacion(reservation, idUser, idPlace);
	}

	@PostMapping(value = "/insert")
	public ResponseEntity<?> newReservation(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestBody Reservation reservation, @RequestParam("place") Long idPlace) {

		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

		if (token.isValidated()) {
			try {
				User user = userService.findByUidFirebase(token.getUid());
				Long idUser = user.getId();
				return new ResponseEntity<Long>(this.reservationService.nuevaReservacion(reservation, idUser, idPlace),
						HttpStatus.OK);

			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Token invalido", HttpStatus.UNAUTHORIZED);
	}

	@GetMapping(value = "/all")
	public Iterable<Reservation> allReservations() {
		return this.reservationService.allReservations();
	}

	@GetMapping(value = "/from-user-sin-token")
	public Iterable<Reservation> reservationsFromUserSinToken(@RequestParam("user") Long id) {
		return this.reservationService.reservationsFromUser(id);
	}

	@GetMapping(value = "/from-user")
	public ResponseEntity<?> reservationsFromUser(@RequestHeader(value = "Authorization") String firebaseToken) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

		if (token.isValidated()) {
			try {
				User user = userService.findByUidFirebase(token.getUid());
				Long idUser = user.getId();
				return new ResponseEntity<Iterable<Reservation>>(this.reservationService.reservationsFromUser(idUser),
						HttpStatus.OK);
//				return this.reservationService.reservationsFromUser(idUser);

			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Token invalido", HttpStatus.UNAUTHORIZED);

	}

	/**
	 * @param firebaseToken
	 * @return Lista de los reservaciones tal que el usuario es el dueño de esos
	 *         lugares
	 */
	@GetMapping(value = "/from-place")
	public ResponseEntity<?> reservationsFromPlace(@RequestHeader(value = "Authorization") String firebaseToken) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

		if (token.isValidated()) {
			try {
				User user = userService.findByUidFirebase(token.getUid());
				Long idUser = user.getId();
				return new ResponseEntity<Iterable<Reservation>>(this.reservationService.reservationsFromPlace(idUser),
						HttpStatus.OK);

			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Token invalido", HttpStatus.UNAUTHORIZED);

	}

	/**
	 * Busca el usuario que realizó la reservación
	 *
	 * @param firebaseToken
	 * @param idReservation
	 * @return El usuario que realizó la reservacion
	 */
	@GetMapping(value = "/user-applicant")
	public ResponseEntity<?> userApplicant(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("reservation") Long idReservation) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

		if (token.isValidated()) {
			try {

				return new ResponseEntity<User>(this.reservationService.getApplicant(idReservation), HttpStatus.OK);

			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Token invalido", HttpStatus.UNAUTHORIZED);
	}

	@GetMapping(value = "/from-place-sin-token")
	public List<Reservation> reservationsFromPlaceSinToken(@RequestParam("user") Long id) {
		return this.reservationService.reservationsFromPlace(id);
	}

	@PostMapping(value = "/confirm")
	public void confirmReservation(@RequestParam("reservation") Long idReservation) {
		final String ESTATUS = "2";
		this.reservationService.confirmReservation(idReservation);
		this.reservationService.updateReservationFirebase(idReservation, ESTATUS);
	}

//	@PostMapping(value = "/pay")
//	public void payReservation(@RequestParam("reservation") Long idReservation) {
//		this.reservationService.payReservation(idReservation);
//	}

	@PostMapping(value = "/cancel")
	public void cancelReservation(@RequestParam("reservation") Long idReservation) throws Exception {
		this.reservationService.cancelReservation(idReservation);
	}

	@PostMapping(value = "/update")
	public ResponseEntity<?> uptateReservation(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("id") Long id, @RequestParam("cost") BigDecimal cost, @RequestParam("hours") int hour) {
		final String ESTATUS = "3";

		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

		if (token.isValidated()) {
			System.out.println("Token valido");
			try {
				this.reservationService.updateReservation(id, ESTATUS, cost, hour);
				this.reservationService.updateReservationFirebase(id, ESTATUS);
				return new ResponseEntity<String>("Updated reservation", HttpStatus.OK);

			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Token invalido", HttpStatus.UNAUTHORIZED);
	}

	@PostMapping(value = "/updateStatus")
	public ResponseEntity<?> uptateReservationStatus(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("id") Long id, @RequestParam("status") String status) {

		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

		if (token.isValidated()) {
			try {
				this.reservationService.updateStatusReservation(id, status);
				this.reservationService.updateReservationFirebase(id, status);
				if (status.equals("5")) {// Reservación pagada
					User user = userService.findByUidFirebase(token.getUid());
					this.reservationService.payReservation(id, user);
					this.reservationService.updateReservationFirebase(id, "5");
				}
				return new ResponseEntity<String>("Updated reservation status", HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Token invalido", HttpStatus.UNAUTHORIZED);
	}

	@GetMapping(value = "/from-id")
	public ResponseEntity<?> getReservationFromId(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("reservation") Long id) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

		if (token.isValidated()) {
			try {
				return new ResponseEntity<ReservationWithEvent>(this.reservationService.getById(id), HttpStatus.OK);

			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}

		return new ResponseEntity<String>("Token invalido", HttpStatus.UNAUTHORIZED);
	}

	@GetMapping(value = "/from-id-with-host")
	public ResponseEntity<?> getReservationFromIdWithHost(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("reservation") Long id) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		if (token.isValidated()) {
			return new ResponseEntity<ReservationWithHostWrapper>(this.reservationService.getReservationWithHost(id),
					HttpStatus.OK);
		}
		return new ResponseEntity<String>("Token invalido", HttpStatus.UNAUTHORIZED);
	}

	@GetMapping(value = "/agenda")
	public ResponseEntity<?> getAgenda(@RequestHeader(value = "Authorization") String firebaseToken) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		if (token.isValidated()) {
			try {

				User user = userService.findByUidFirebase(token.getUid());
				return new ResponseEntity<List<OffsetDateTime>>(this.reservationService.getAgenda(user.getId()),
						HttpStatus.OK);

			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Error", HttpStatus.UNAUTHORIZED);

	}

	@GetMapping(value = "/agenda-por-dia")
	public ResponseEntity<?> getAgendaPorDia(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("year") int year, @RequestParam("month") int month, @RequestParam("day") int day) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		if (token.isValidated()) {
			try {
				User user = userService.findByUidFirebase(token.getUid());
				return new ResponseEntity<List<ReservationWithEvent>>(
						this.reservationService.getAgendaPorDia(user.getId(), year, month, day), HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Error", HttpStatus.UNAUTHORIZED);
	}

	@GetMapping(value = "/agenda-por-dia-sin-token")
	public ResponseEntity<?> getAgendaPorDiaSonToken(@RequestParam("year") int year, @RequestParam("month") int month,
			@RequestParam("day") int day, @RequestParam("user") Long idUser) {

		try {
			User user = userService.getById(idUser);
			return new ResponseEntity<List<ReservationWithEvent>>(
					this.reservationService.getAgendaPorDia(user.getId(), year, month, day), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
		}

//		return new ResponseEntity<String>("Error", HttpStatus.UNAUTHORIZED);
	}

	@GetMapping(value = "/agenda-sin-token")
	public ResponseEntity<?> getAgendaSinToken(@RequestParam("user") Long id) {
		try {

			return new ResponseEntity<List<OffsetDateTime>>(this.reservationService.getAgenda(id), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping(value = "/deleteFirebase")
	public void deleteFirebase() {
		// this.reservationService.deleteReservationFirebase();
	}

	/*
	 * @GetMapping(value = "/getById") public ResponseEntity<?>
	 * getReservationFromId(
	 * 
	 * @RequestParam("reservation") Long id) { ReservationWithEvent reservation =
	 * this.reservationService.getById(id); if (reservation != null) { return new
	 * ResponseEntity<ReservationWithEvent>(reservation, HttpStatus.OK); } else {
	 * return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST); }
	 * 
	 * }
	 * 
	 * @GetMapping(value = "/getByTicket") public ResponseEntity<?>
	 * getReservationFromTicket() { return new
	 * ResponseEntity<List<Reservation>>(this.reservationService.getbyTicket(),
	 * HttpStatus.OK); }
	 */

}
