package com.partyOn.partyOn_backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.partyOn.partyOn_backend.model.Events;
import com.partyOn.partyOn_backend.service.EventService;

@RestController
@RequestMapping({ "/event/" })
public class EventController {

	@Autowired
	private EventService eventService;

	@GetMapping(value = "all")
	public Iterable<Events> getAll() {
		return eventService.all();
	}

	@GetMapping(value = "from-user")
	public List<Events> eventsFromUser(@RequestParam("user") Long id) {
		return this.eventService.fromUser(id);
	}

	@GetMapping(value = "from-place")
	public List<Events> eventsFromPlcae(@RequestParam("place") Long id) {
		return this.eventService.fromPlace(id);
	}

	@PostMapping(value = "add-extra")
	public void addExtra(@RequestParam("event") Long idEvent, @RequestParam("extra") Long idExtra) {
		this.eventService.addExtra(idEvent, idExtra);
	}

	@PostMapping(value = "delete-extra")
	public void deleteExtra(@RequestParam("event") Long idEvent, @RequestParam("extra") Long idExtra) {
		this.eventService.deleteExtra(idEvent, idExtra);
	}
}
