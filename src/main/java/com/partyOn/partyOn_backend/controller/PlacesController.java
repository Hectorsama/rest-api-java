package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.jsonAdapter.PlaceDetailsFavWrapper;
import com.partyOn.partyOn_backend.jsonAdapter.PlaceRequestWrapper;
import com.partyOn.partyOn_backend.model.*;
import com.partyOn.partyOn_backend.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping({ "/place" })
public class PlacesController {
	@Autowired
	PlaceService placeService;
	@Autowired
	FireBaseService fireBaseService;
	@Autowired
	PhotosService photosService;
	@Autowired
	BusyDayService busyDayService;
	@Autowired
	PropertyTypeService propertyTypeService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public ResponseEntity<?> insert_places(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestBody PlaceRequestWrapper placeRequestWrapper) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		if (token.isValidated()) {
			try {
				User user = userService.findByUidFirebase(token.getUid());
				Long idUser = user.getId();
				this.placeService.insert(placeRequestWrapper, idUser);
				return new ResponseEntity<String>("Store place successfully", HttpStatus.OK);

			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Token Error", HttpStatus.UNAUTHORIZED);
	}

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<?> getAll() {
		try {
			return new ResponseEntity<Iterable<Place>>(this.placeService.getAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/by-id", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("place") Long id) {
		// System.out.println(firebaseToken);
		String header = firebaseToken.trim();
		if (!header.equals("Bearer")) {
			TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
			if (token.isValidated()) {
				try {
					System.out.println("Conectado");
					User user = userService.findByUidFirebase(token.getUid());
					return new ResponseEntity<PlaceDetailsFavWrapper>(this.placeService.getByIdDetails(id, user, true),
							HttpStatus.OK);
				} catch (Exception e) {
					return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
				}
			}
		} else {
			System.out.println("No conectado");
			return new ResponseEntity<PlaceDetailsFavWrapper>(this.placeService.getByIdDetails(id, null, false),
					HttpStatus.OK);

		}
		return new ResponseEntity<String>("Token Error", HttpStatus.UNAUTHORIZED);
	}

	@RequestMapping(value = "/getByCategory", method = RequestMethod.GET)
	public ResponseEntity<?> getByCategoy(@RequestParam("name") String name, @RequestParam("latitud") Double latitud,
			@RequestParam("longitud") Double longitud, @RequestParam("radio") int radioKm) {
		try {
			return new ResponseEntity<List<Place>>(this.placeService.getByCategory(name, latitud, longitud, radioKm),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/getByLocation", method = RequestMethod.GET)
	public ResponseEntity<?> getByLocation(@RequestParam("latitud") Double latitud,
			@RequestParam("longitud") Double longitud, @RequestParam("radio") int radioKm) {
		try {
			return new ResponseEntity<List<Place>>(this.placeService.getByLocation(latitud, longitud, radioKm),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
	}

//	@GetMapping(value = "/add-timezone-id")
//	public void getTimeZone() {
//		this.placeService.getTimeZone();
//	}

	
	
	/**
	 * Devuelve un recinto dado un identificador
	 * @param idPlace
	 * @return
	 */
	


}
