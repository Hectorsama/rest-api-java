package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.jsonAdapter.ReservationWithEvent;
import com.partyOn.partyOn_backend.model.Reservation;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.service.ReservationService;
import com.partyOn.partyOn_backend.service.TransferService;
import com.partyOn.partyOn_backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping({"/transfer"})
public class TranferController {

    @Autowired
    private TransferService transferService;
    @Autowired
    FireBaseService fireBaseService;
    @Autowired
    UserService userService;

    @Autowired
    ReservationService reservationService;


    @PostMapping(value = "/update")
    public ResponseEntity<?> newReservation(@RequestHeader(value = "Authorization") String firebaseToken,
                                            @RequestParam("id") Long id, @RequestParam("cost") BigDecimal cost, @RequestParam("hours") int hours) {


        TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

        if (token.isValidated()) {
            try {

                this.transferService.updateStatusTicket(id, cost, hours);
                return new ResponseEntity<String>("OK", HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<String>("Error " + e, HttpStatus.BAD_REQUEST);
            }

        }
        return new ResponseEntity<String>("Error ", HttpStatus.BAD_REQUEST);
    }


    @PostMapping(value = "/insert")
    public ResponseEntity<?> insertPayment(@RequestHeader(value = "Authorization") String firebaseToken,
                                           @RequestParam("id") Long id) {


        TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

        if (token.isValidated()) {
            try {
                User user = userService.findByUidFirebase(token.getUid());
                Long idUser = user.getId();
                this.transferService.insertPayment(id, idUser);
                return new ResponseEntity<String>("OK", HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<String>("Error " + e, HttpStatus.BAD_REQUEST);
            }

        }
        return new ResponseEntity<String>("Error ", HttpStatus.BAD_REQUEST);
    }

    /*ADMIN*/
   /* @PostMapping(value = "/updatePayment")
    public ResponseEntity<?> paymentUpdated(@RequestParam("reservartion") Long id) {

        try {
            ReservationWithEvent reservation = this.reservationService.getById(id);
            User u = reservation.getReservation().findApplicant();
            this.reservationService.payReservation(id, u);
            //Actualizar estatus pagado en firebase
            this.reservationService.updateReservationFirebase(id, "5");
            return new ResponseEntity<String>("OK", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
        }

    }*/

}
