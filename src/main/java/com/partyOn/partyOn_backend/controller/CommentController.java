package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.mail.MailService;
import com.partyOn.partyOn_backend.mail.Mails;
import com.partyOn.partyOn_backend.model.BusyDay;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping({"/comment"})
public class CommentController {
    @Autowired
    private Environment env;
    @Autowired
    FireBaseService fireBaseService;
    @Autowired
    MailService mailService;
    @Autowired
    private UserService userService;

    @PostMapping(value = "/send")
    public ResponseEntity<?> sendComment(@RequestHeader(value = "Authorization") String firebaseToken, @RequestParam("comment") String comment, @RequestParam("subject") String subject) {
        TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
        if (token.isValidated()) {
            try {

                System.out.println("------------->" + comment);
                User user = userService.findByUidFirebase(token.getUid());
                final String mail_env = env.getProperty("MAIL_PARTYON");
                Mails mail = new Mails();
                mail.setMailFrom(user.getEmail());
                mail.setMailTo(mail_env);
                mail.setMailSubject(subject);
                Map<String, Object> model = new HashMap<String, Object>();
                model.put("user", user.getEmail());
                model.put("comment", comment);
                mail.setModel(model);

                mailService.sendEmailComment(mail);
                return new ResponseEntity<String>("Mail send", HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
            }
        }

        return new ResponseEntity<String>("Token Error", HttpStatus.UNAUTHORIZED);
    }
}
