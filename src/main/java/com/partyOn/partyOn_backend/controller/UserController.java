package com.partyOn.partyOn_backend.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.model.ValueParty;
import com.partyOn.partyOn_backend.repo.ValuePartyRepo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.partyOn.partyOn_backend.model.Category;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.repo.UserRepo;
import com.partyOn.partyOn_backend.service.UserService;

@RestController
@RequestMapping({ "/user" })
public class UserController {
	@Autowired
	UserService userService;

	@Autowired
	FireBaseService fireBaseService;

	@Autowired
	private UserRepo userRepo;
	@Autowired
	private ValuePartyRepo valuePartyRepo;

	private static final Log log = LogFactory.getLog(UserController.class);

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public ResponseEntity<?> nuevoUsuario(@RequestBody User user) {
		try {
			this.userService.nuevoUsuario(user);
			return new ResponseEntity<String>("Agregado con exito", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<?> getAll() {
		try {
			return new ResponseEntity<Iterable<User>>(this.userService.users(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable("id") Long id) {
		try {
			return new ResponseEntity<User>(this.userService.getById(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public ResponseEntity<?> getByIdAuth(@RequestHeader(value = "Authorization") String firebaseToken) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		if (token.isValidated()) {
			try {
				User user = userService.findByUidFirebase(token.getUid());
				Long id = user.getId();
				return new ResponseEntity<User>(this.userService.getById(id), HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("USER NOT FOUND", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Error", HttpStatus.UNAUTHORIZED);
	}

	@DeleteMapping(value = "/{id}")
	public void deleteUser(@PathVariable("id") Long id) {
		this.userService.deleteUser(id);
	}

	@GetMapping(value = "/propietario")
	public User propietario(@RequestParam("place") Long idPlace) {
		return this.userService.propietario(idPlace);
	}

	@PostMapping(value = "/check-ine")
	public ResponseEntity<?> checkCode(@RequestHeader(value = "Authorization") String firebaseToken) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		if (token.isValidated()) {
			try {
				User user = userService.findByUidFirebase(token.getUid());
				Long idUser = user.getId();
				this.userService.uptateStatus(idUser);
				return new ResponseEntity<String>("Documentation store ", HttpStatus.OK);

			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Token Error", HttpStatus.UNAUTHORIZED);
	}

	@PostMapping(value = "/img-profile")
	public ResponseEntity<?> updateProfilePhoto(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("url") String imgUrl) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

		if (token.isValidated()) {
			try {
				User user = userService.findByUidFirebase(token.getUid());
				this.userService.updateImageProfile(user.getId(), imgUrl);
				return new ResponseEntity<String>("imgProfile Updated ", HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}

		return new ResponseEntity<String>("Token Error", HttpStatus.UNAUTHORIZED);
	}
}
