package com.partyOn.partyOn_backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.partyOn.partyOn_backend.model.AdditionalService;
import com.partyOn.partyOn_backend.model.AdditionalServiceCategory;
import com.partyOn.partyOn_backend.service.AdditionalServiceCategoryService;

@RestController
@RequestMapping({"/additional-service-category"})
public class AdditionalServiceCategoryController {
	@Autowired
	AdditionalServiceCategoryService additionalServiceCategoryService;
	
	@RequestMapping(value = "/by-category", method = RequestMethod.GET)
    public ResponseEntity<?> getAllExtras(@RequestParam("subId") String subId) {
		try {
			AdditionalServiceCategory categoriaServicio= additionalServiceCategoryService.findSubId(subId);
			return new ResponseEntity<List<AdditionalService>>(categoriaServicio.allAdditionalServices(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
	}
	
}
