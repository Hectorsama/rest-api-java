package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.model.Category;
import com.partyOn.partyOn_backend.model.Services;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.service.CategoryService;
import com.partyOn.partyOn_backend.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({"/service"})
public class ServiceController {
    @Autowired
    ServiceService service;

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResponseEntity<?> insert_service(@RequestBody Services service) {
        try {
            this.service.insert(service);
            return new ResponseEntity<String>("Store service succesfully", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        try {
            return new ResponseEntity<Iterable<Services>>(this.service.getAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "getById/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity<Services>(this.service.getById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
        }
    }
}