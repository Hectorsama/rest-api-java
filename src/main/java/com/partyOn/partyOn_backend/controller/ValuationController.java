package com.partyOn.partyOn_backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.partyOn.partyOn_backend.service.ValuationService;

@RestController
@RequestMapping({ "/valuation" })
public class ValuationController {

	@Autowired
	private ValuationService valuationService;

	@PostMapping(value = "/add")
	public void insertValuation(@RequestParam("place") Long idPlace, @RequestParam("event") Long idEvent,@RequestParam("rating") int rating) {
		this.valuationService.addValuation(idPlace, idEvent,rating);
	}
}
