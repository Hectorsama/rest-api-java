package com.partyOn.partyOn_backend.controller;


import com.twilio.Twilio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
@RestController
@RequestMapping({"/message"})
public class MessageController {
    @Autowired
    private Environment env;

    @PostMapping("/sendSMS")
    private void sendSMS()  {
        final String ACCOUNT_SID = env.getProperty("ACCOUNT_SID");
        final String AUTH_TOKEN =   env.getProperty("AUTH_TOKEN");
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Message message = Message
                .creator(new PhoneNumber("+525582344682"), // to
                        new PhoneNumber("+14437752210"), // from
                        "Berna , ya hiciste los deberes?")
                .create();

    }




}
