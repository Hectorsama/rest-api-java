package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.repo.SmsVerificationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.model.ValueParty;
import com.partyOn.partyOn_backend.repo.UserRepo;
import com.partyOn.partyOn_backend.repo.ValuePartyRepo;
import com.partyOn.partyOn_backend.service.SmsVerificationService;
import com.partyOn.partyOn_backend.service.UserService;

@RestController
@RequestMapping({ "/sms-verification" })
public class SmsVerificationController {

	@Autowired
	private SmsVerificationService smsVerificationService;

	@Autowired
	FireBaseService fireBaseService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private SmsVerificationRepo smsVerificationRepo;

	@Autowired
	private ValuePartyRepo valuePartyRepo;

	@PostMapping(value = "/genera-codigo")
	public ResponseEntity<?> generaCodigo(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("numero") String numeroTel) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));

		if (token.isValidated()) {
			try {
				User user = userService.findByUidFirebase(token.getUid());// this.userRepo.findById(1L).orElse(null);
				String exist = this.smsVerificationRepo.findNumber(numeroTel);
				if (exist == null) {
					this.smsVerificationService.generaCodigo(user, numeroTel);
					return new ResponseEntity<String>("Listo", HttpStatus.OK);
				} else {
					return new ResponseEntity<String>("Número registrado con otro usuario ", HttpStatus.IM_USED);
				}

			} catch (Exception e) {
				return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Token invalido", HttpStatus.UNAUTHORIZED);
	}

	@PostMapping(value = "/check-code")
	public ResponseEntity<?> checkCode(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("code") String code, @RequestParam("number") String numeroTel) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		if (token.isValidated()) {
			try {
				User user = userService.findByUidFirebase(token.getUid());
				Long idUser = user.getId();
				System.out.println("El codigo recibido es: " + code);
				String codeDb = this.smsVerificationService.getCode(idUser);
				System.out.println("El codigo de la BD es: " + codeDb);
				if (codeDb.equals(code)) {
					ValueParty valueParty = this.valuePartyRepo.findByTableAndKey("usuarios", "pendiente_ine");
					user.setStatus(valueParty.getValue());
					user.setPhone(numeroTel);
					this.smsVerificationService.deleteCode(user);
					return new ResponseEntity<String>("Valid code", HttpStatus.OK);
				}
				return new ResponseEntity<String>("Invalid Code", HttpStatus.BAD_REQUEST);
			} catch (Exception e) {
				e.printStackTrace();
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		}
		return new ResponseEntity<String>("Token Error", HttpStatus.UNAUTHORIZED);
	}

	@PostMapping(value = "/verify-number")
	public ResponseEntity<?> verifyNumber(/* @RequestHeader(value = "Authorization") String firebaseToken, */
			@RequestParam("numero") String numeroTel) {
//        TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		System.out.println("------>" + numeroTel);
//        if (token.isValidated()) {
		try {

			String exist = this.smsVerificationRepo.findNumber(numeroTel);
			System.out.println("------>" + exist);
			if (exist == null) {
				return new ResponseEntity<String>("Listo", HttpStatus.OK);
			} else {
				return new ResponseEntity<String>("Error numero ya existe ", HttpStatus.BAD_REQUEST);
			}

		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
//        }
//        return new ResponseEntity<String>("Token invalido", HttpStatus.UNAUTHORIZED);
	}

}
