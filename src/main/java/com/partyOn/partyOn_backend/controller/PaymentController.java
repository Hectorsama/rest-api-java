package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.model.Reservation;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.repo.PaymentRepo;
import com.partyOn.partyOn_backend.repo.ReservationRepo;
import com.partyOn.partyOn_backend.service.ReservationService;
import com.partyOn.partyOn_backend.service.UserService;
import com.partyOn.partyOn_backend.util.ConverterDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.partyOn.partyOn_backend.model.Payment;
import com.partyOn.partyOn_backend.service.PaymentService;

import java.time.Clock;
import java.time.OffsetDateTime;

@RestController
@RequestMapping({ "/payment" })
public class PaymentController {

	@Autowired
	private PaymentService paymentService;
	@Autowired
	private ReservationService reservationService;

	@Autowired
	private ReservationRepo reservationRepo;
	@Autowired
	private PaymentRepo paymentRepo;

	@Autowired
	private UserService userService;
	@Autowired
	FireBaseService fireBaseService;

	@PostMapping(value = "/insert")
	/**
	 * Ejemplo de como crear una un nuevo pago
	 */
	public void newPayment() {
		Payment payment = new Payment();
		Reservation reservation = this.reservationRepo.findById(91L).orElse(null);

		Long id_user = 3L;
		payment.setOrderNumber("order-0003");
		payment.setReservation(reservation);
		OffsetDateTime offsetdatetime = OffsetDateTime.now(Clock.systemUTC());
		payment.setPayDay(offsetdatetime);
		this.paymentService.add(payment, id_user);
	}

	@GetMapping("/from-user")
	public Iterable<Payment> paymentFromUser(@RequestParam("user") Long id) {
		return this.paymentService.getPaymentFromUser(id);
	}

	@GetMapping("/getPayment/{id}")
	public Payment paymentAll(@PathVariable("id") Long id) {
		return this.paymentService.getById(id);
	}

	@GetMapping("/getByUser")
	public Iterable<Payment> paymentUser(@RequestHeader(value = "Authorization") String firebaseToken) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		if (token.isValidated()) {
			User user = userService.findByUidFirebase(token.getUid());
			Long id = user.getId();
			return this.paymentService.getPaymentFromUser(id);
		}
		return null;
	}

	@GetMapping("/from-reservation")
	public ResponseEntity<?> getPaymentByIdReservation(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("id") Long idReservation) {

		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		if (token.isValidated()) {
			try {
				System.out.println("----------->"+idReservation);
				Payment paymentRequest = this.paymentRepo.findByIdReservation(idReservation);
				Reservation reservation= paymentRequest.getReservation();
				String zonaHoraria= reservation.getRequestedPlace().getTimeZone();
				OffsetDateTime diaEventoConTiempoLocal = ConverterDate.toZoneIdFromUTC(reservation.getEventDay(), zonaHoraria);
				OffsetDateTime diaReservacionTiempoLocal= ConverterDate.toZoneIdFromUTC(reservation.getReservationDay(), zonaHoraria);

				paymentRequest.getReservation().setEventDay(diaEventoConTiempoLocal);
				paymentRequest.getReservation().setReservationDay(diaReservacionTiempoLocal);
				
				System.out.println("----------->"+paymentRequest);
				return new ResponseEntity<Payment>(paymentRequest, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
			}
		} else {
			return new ResponseEntity<String>("Error", HttpStatus.UNAUTHORIZED);
		}
	}
}
