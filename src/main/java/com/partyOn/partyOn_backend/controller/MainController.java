package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.model.Category;
import com.partyOn.partyOn_backend.repo.CategoryRepo;

import com.partyOn.partyOn_backend.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class MainController {
    @Autowired
    CategoryRepo category;
    @Autowired
    ReservationService reservationService;


    @RequestMapping(value = "/hola", method = RequestMethod.GET)
    public Collection<Category> getAllCategories() {
        return (Collection<Category>) category.findAll();
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResponseEntity<?> insert(@RequestBody Category c) {
        try {
            category.save(c);
            return new ResponseEntity<String>("Category store", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error" + e, HttpStatus.BAD_REQUEST);

        }

    }


    /*@RequestMapping(value = "/time", method = RequestMethod.GET)
    public void getCountTime() {
        this.reservationService.setTypeReservation();
    }*/
}