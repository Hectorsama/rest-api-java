package com.partyOn.partyOn_backend.controller;


import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.jsonAdapter.ReservationWithEvent;
import com.partyOn.partyOn_backend.model.PaymentPlace;
import com.partyOn.partyOn_backend.model.Place;
import com.partyOn.partyOn_backend.model.Reservation;
import com.partyOn.partyOn_backend.model.User;
import com.partyOn.partyOn_backend.service.PaymentPlaceService;
import com.partyOn.partyOn_backend.service.PlaceService;
import com.partyOn.partyOn_backend.service.ReservationService;
import com.partyOn.partyOn_backend.service.TransferService;
import com.partyOn.partyOn_backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping({ "/v2/admin" })
public class AdminController {

    @Autowired
    private TransferService transferService;
    @Autowired
    FireBaseService fireBaseService;
    @Autowired
    UserService userService;
    @Autowired
	PlaceService placeService;
    @Autowired
    ReservationService reservationService;
    @Autowired
	private PaymentPlaceService paymentService;


    /***
     *Actualiza el estatus de la reservación a pagado , se actualiza en firebase para notificar
     * @param id reservation
     * @return ResponseEntity
     */
    @PostMapping(value = "/updatePayment")
    public ResponseEntity<?> paymentUpdated(@RequestParam("reservation") Long id) {

        try {
            ReservationWithEvent reservation = this.reservationService.getById(id);
            User u = reservation.getReservation().findApplicant();
            this.reservationService.payReservation(id, u);
            //Actualizar estatus pagado en firebase
            this.reservationService.updateReservationFirebase(id, "5");
            return new ResponseEntity<String>("OK", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
        }

    }


    /***
     *Regresa una reservación
     * @param id reservation
     * @return Reservation
     */
    @GetMapping(value = "/getReservationById")
    public ResponseEntity<?> getReservationFromId(
            @RequestParam("reservation") Long id) {
        ReservationWithEvent reservation = this.reservationService.getById(id);
        if (reservation != null) {
            return new ResponseEntity<ReservationWithEvent>(reservation, HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("Error", HttpStatus.BAD_REQUEST);
        }

    }
    /***
     *Regresa una lista de reservaciones con estatus ticket generado
     * @param
     * @return Reservation List with ticket status
     */
    @GetMapping(value = "/getReservationByTicket")
    public ResponseEntity<?> getReservationFromTicket() {
        return new ResponseEntity<List<Reservation>>(this.reservationService.getbyTicket(),HttpStatus.OK);
    }
    
    @GetMapping(value = "/recintos-por-revisar-pago")
	public ResponseEntity<?> getRecintosPorPagar() {
		try {
			return new ResponseEntity<List<Place>>(this.placeService.recintosPorRevisar(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
	}
    
    
    @GetMapping(value = "/detail-place")
	public ResponseEntity<?> getById(@RequestParam("id") Long idPlace) {
		try {
			Place place= this.placeService.getById(idPlace);
			return new ResponseEntity<Place>(place, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping(value = "/activa-recinto")
	public ResponseEntity<?> activaRecinto(@RequestParam("id") Long idPlace) {
		try {
			Place place= this.placeService.getById(idPlace);
			this.placeService.activaRecinto(idPlace);
			return new ResponseEntity<String>("Recinto activado", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping(value = "/payment-by-place")
	public PaymentPlace getByPlace(@RequestParam("id") Long idPlace) {
		return this.paymentService.getByPlace(idPlace);
	}
}
