package com.partyOn.partyOn_backend.controller;

import com.partyOn.partyOn_backend.config.auth.FireBaseService;
import com.partyOn.partyOn_backend.config.auth.TokenInfo;
import com.partyOn.partyOn_backend.model.BusyDay;
import com.partyOn.partyOn_backend.model.Category;
import com.partyOn.partyOn_backend.service.BusyDayService;
import com.partyOn.partyOn_backend.service.CategoryService;

import java.time.ZonedDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping({ "/busy-days" })
public class BusyDayController {
	@Autowired
	BusyDayService busyDayService;

	@Autowired
	FireBaseService fireBaseService;
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public ResponseEntity<?> insert_day(@RequestBody BusyDay busyDay, @RequestParam("id_place") Long id) {
		try {

			this.busyDayService.insert(busyDay, id);
			return new ResponseEntity<String>("Store busyday succesfully", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity<?> all() {
		try {

			return new ResponseEntity<Iterable<BusyDay>>(this.busyDayService.getAll(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(value = "/from-place")
	public ResponseEntity<?> fromPlace(@RequestHeader(value = "Authorization") String firebaseToken,
			@RequestParam("place") Long idPlace) {
		TokenInfo token = this.fireBaseService.parseToken(firebaseToken.replace("Bearer ", ""));
		if (token.isValidated()) {
			try {
				return new ResponseEntity<Iterable<BusyDay>>(this.busyDayService.getBusyDaysFromPlace(idPlace),
						HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
			}
		}
		
		return new ResponseEntity<String>("Token Error", HttpStatus.UNAUTHORIZED);
	}

}
