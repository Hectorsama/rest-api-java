package com.partyOn.partyOn_backend.util;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class ConverterDate {

	/**
	 * Convierte una fecha que viene en UTC a la zona horaria indicada para que 
	 * a partir de esa fecha lo devuelva en UTC 
	 * @param date
	 * @param timeZone
	 * @return La fecha de un lugar dada una zona horaria aplicandole UTC.
	 */
	static public OffsetDateTime toZoneIdInUTC(OffsetDateTime date, String timeZone) {
		ZoneId zoneIdPlace = ZoneId.of(timeZone);
		OffsetDateTime localDate = OffsetDateTime.now(zoneIdPlace);
		OffsetDateTime dateWithLocalTime = localDate.withYear(date.getYear()).withMonth(date.getMonthValue())
				.withDayOfMonth(date.getDayOfMonth()).withHour(date.getHour()).withMinute(date.getMinute())
				.withSecond(date.getSecond());
		
		//System.out.println("ZoneId offset:" + zoneIdLugar.getRules().getOffset(Instant.now())); // Dado una ZoneId
		// obtengo el offset
		// "+5:00" etc
		return dateWithLocalTime.withOffsetSameInstant(ZoneOffset.UTC);
	}
	
	/**
	 * A partir de una fecha en UTC devuelve el horario con offset
	 * @param date
	 * @param timeZone
	 * @return
	 */
	static public OffsetDateTime toZoneIdFromUTC(OffsetDateTime date, String timeZone) {
		ZoneId zoneIdPlace = ZoneId.of(timeZone);
		ZoneOffset offsetFromPlace= zoneIdPlace.getRules().getOffset(Instant.now());
		return date.withOffsetSameInstant(offsetFromPlace);
	}

}
