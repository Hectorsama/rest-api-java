package com.partyOn.partyOn_backend.mail;

import com.partyOn.partyOn_backend.model.AdditionalServiceCategory;
import com.partyOn.partyOn_backend.model.Reservation;
import com.partyOn.partyOn_backend.repo.ReservationRepo;
import com.partyOn.partyOn_backend.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping({ "/email-partyon" })
public class EmailsController {
    @Autowired
    MailService mailService;
    @Autowired
    private Environment env;
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private ReservationRepo reservationRepo;

    @RequestMapping(value = "/send", method = RequestMethod.GET)
    public ResponseEntity<?> getAllExtras() {
        try {

            final String mail_env = env.getProperty("MAIL_PARTYON");
            Mails mail = new Mails();
            mail.setMailFrom(mail_env);
            mail.setMailTo("santaellamarin@gmail.com");
            mail.setMailSubject("Reservación de un recinto");
            Reservation reservation = this.reservationRepo.findById(161L).orElse(null);
            Map<String, Object> model = new HashMap<String, Object>();
            model.put("dia", reservation.getEventDay().getDayOfMonth());
            model.put("mes", reservation.getEventDay().getMonthValue());
            model.put("year", reservation.getEventDay().getYear());
            model.put("date", reservation.getEventDay());
            model.put("name", reservation.getRequestedPlace().findUser().getName());
            model.put("name_place", reservation.getRequestedPlace().getName());
            model.put("price", reservation.getTotalCost().toString());
            model.put("mail", reservation.getRequestedPlace().findUser().getEmail());
            mail.setModel(model);
            mailService.sendEmailPlaceUser(mail);
            // mailService.sendEmailPlaceOwner(mail);

            return new ResponseEntity<String>("Store", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<String>("Error: " + e, HttpStatus.BAD_REQUEST);
        }
    }
}

