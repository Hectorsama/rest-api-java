package com.partyOn.partyOn_backend;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication
public class PartyOnBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PartyOnBackendApplication.class, args);
	}
	
	 @PostConstruct
	    void started() {
	        // set JVM timezone as UTC
	        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	    }

}
