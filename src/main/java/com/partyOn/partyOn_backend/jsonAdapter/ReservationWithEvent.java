package com.partyOn.partyOn_backend.jsonAdapter;

import com.partyOn.partyOn_backend.model.AdditionalServiceReservation;
import com.partyOn.partyOn_backend.model.Events;
import com.partyOn.partyOn_backend.model.Reservation;

public class ReservationWithEvent {

	public static final int ES_MI_RESERVACION = 1;
    public static final int SOY_INVITADO = 2;
	public static final int ES_RESERVACION_EXTRA = 3;

	private int statusConsulta;
	private Reservation reservation;
	private AdditionalServiceReservation extraReservation;
	private Events event;

	public ReservationWithEvent(int statusConsulta, Reservation reservation) {
		this.statusConsulta = statusConsulta;
		this.reservation = reservation;
	}
	
	public ReservationWithEvent(int statusConsulta, AdditionalServiceReservation additionalServiceReservation) {
		this.statusConsulta = statusConsulta;
		this.extraReservation= additionalServiceReservation;
	}
	
	public ReservationWithEvent(int statusConsulta, Reservation reservation,Events event) {
		this.statusConsulta = statusConsulta;
		this.reservation = reservation;
		this.event= event;
	}

	
	public AdditionalServiceReservation getExtraReservation() {
		return extraReservation;
	}

	public void setExtraReservation(AdditionalServiceReservation extraReservation) {
		this.extraReservation = extraReservation;
	}

	public int getStatusConsulta() {
		return statusConsulta;
	}

	public void setStatusConsulta(int statusConsulta) {
		this.statusConsulta = statusConsulta;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public Events getEvent() {
		return event;
	}

	public void setEvent(Events event) {
		this.event = event;
	}
	
	

}
