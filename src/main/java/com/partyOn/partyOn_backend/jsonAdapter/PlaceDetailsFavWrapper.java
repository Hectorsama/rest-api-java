package com.partyOn.partyOn_backend.jsonAdapter;

import com.partyOn.partyOn_backend.model.Place;
import com.partyOn.partyOn_backend.model.User;

public class PlaceDetailsFavWrapper {

	private Place place;
	private Boolean isFav;
	private User owner;
	public Place getPlace() {
		return place;
	}
	public void setPlace(Place place) {
		this.place = place;
	}
	public Boolean getIsFav() {
		return isFav;
	}
	public void setIsFav(Boolean isFav) {
		this.isFav = isFav;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	
	
}
