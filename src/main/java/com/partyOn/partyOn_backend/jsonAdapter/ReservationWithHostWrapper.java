package com.partyOn.partyOn_backend.jsonAdapter;

import com.partyOn.partyOn_backend.model.Events;
import com.partyOn.partyOn_backend.model.Reservation;
import com.partyOn.partyOn_backend.model.User;

public class ReservationWithHostWrapper {

	//private Reservation reservation;
	private User host;
	private Events event;
	
	public Events getEvent() {
		return event;
	}
	public void setEvent(Events event) {
		this.event = event;
	}
	//	public Reservation getReservation() {
//		return reservation;
//	}
//	public void setReservation(Reservation reservation) {
//		this.reservation = reservation;
//	}
	public User getHost() {
		return host;
	}
	public void setHost(User host) {
		this.host = host;
	}
	
	
	
}
