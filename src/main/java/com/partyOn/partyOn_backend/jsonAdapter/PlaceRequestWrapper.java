package com.partyOn.partyOn_backend.jsonAdapter;

import com.partyOn.partyOn_backend.model.*;

import java.util.List;

public class PlaceRequestWrapper {
    private Place place;
    private List<Category> category;
    private List<Services> services;
    private List<Photos> photos;
    private List<BusyDay> busyDays;
    private PropertyType propertyType;
    private PackageParty packageParty;

    public PackageParty getPackageParty() {
        return packageParty;
    }

    public void setPackageParty(PackageParty packageParty) {
        this.packageParty = packageParty;
    }

    public PropertyType getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType) {
        this.propertyType = propertyType;
    }

    public List<BusyDay> getBusyDays() {
        return busyDays;
    }

    public void setBusyDays(List<BusyDay> busyDays) {
        this.busyDays = busyDays;
    }

    public List<Services> getServices() {
        return services;
    }

    public List<Photos> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photos> photos) {
        this.photos = photos;
    }

    public void setServices(List<Services> services) {
        this.services = services;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "PlaceRequestWrapper{" +
                "place=" + place +
                ", category=" + category +
                ", services=" + services +
                ", photos=" + photos +
                ", busyDays=" + busyDays +
                ", propertyType=" + propertyType +
                ", packageParty=" + packageParty +
                '}';
    }
}
