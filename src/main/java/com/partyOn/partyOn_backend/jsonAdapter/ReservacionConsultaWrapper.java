package com.partyOn.partyOn_backend.jsonAdapter;

import java.time.OffsetDateTime;

public class ReservacionConsultaWrapper {

	private int statusConsulta;
	
	private OffsetDateTime eventDay;
	
	private Long idReservation;

	public int getStatusConsulta() {
		return statusConsulta;
	}

	public void setStatusConsulta(int statusConsulta) {
		this.statusConsulta = statusConsulta;
	}

	public OffsetDateTime getEventDay() {
		return eventDay;
	}

	public void setEventDay(OffsetDateTime eventDay) {
		this.eventDay = eventDay;
	}

	public Long getIdReservation() {
		return idReservation;
	}

	public void setIdReservation(Long idReservation) {
		this.idReservation = idReservation;
	}

	public ReservacionConsultaWrapper(int statusConsulta, OffsetDateTime eventDay, Long idReservation) {
		this.statusConsulta = statusConsulta;
		this.eventDay = eventDay;
		this.idReservation= idReservation;
	}
	
	
	
	
	
}
